// Copyright 2018-2019 Zedge, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and

package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"gitlab.com/zedge-oss/zeppa/event-taxonomy/pkg/model"
)

var updateFlag bool

func init() {
	flag.BoolVar(&updateFlag, "update", false, "validate a taxonomy update, requires two file names")
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage: validate-taxonomy [{file} | -update {old_file} {new_file}]\n")
	}
	flag.Parse()
	if updateFlag {
		if flag.NArg() != 2 {
			flag.Usage()
			os.Exit(1)
		}
		oldFile := flag.Args()[0]
		newFile := flag.Args()[1]
		changes, err := model.ComputeChangesFromFiles(oldFile, newFile)
		if err != nil {
			log.Fatal(err)
		}
		numIssues := 0
		for _, issue := range changes.Validate() {
			fmt.Fprintf(os.Stderr, "%s: %v\n", newFile, issue)
			numIssues++
		}
		if numIssues > 0 {
			os.Exit(2)
		}
	} else {
		if flag.NArg() != 1 {
			flag.Usage()
			os.Exit(1)
		}
		file := flag.Args()[0]
		_, err := model.LoadTaxonomyFromFile(file)
		if err != nil {
			log.Fatal(fmt.Errorf("%s: %v", file, err))
		}
	}
}
