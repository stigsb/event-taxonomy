// Copyright 2018-2019 Zedge, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and

package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"gitlab.com/zedge-oss/zeppa/event-taxonomy/pkg/apis/dwh/v1beta1"
	"gitlab.com/zedge-oss/zeppa/event-taxonomy/pkg/clickhouse"
	"gitlab.com/zedge-oss/zeppa/event-taxonomy/pkg/controllers"
)

func main() {
	flag.Parse()

	if len(os.Args) <= 1 {
		log.Fatal("give kubeObj json file as only parameter\n")
	}
	file, err := ioutil.ReadFile(os.Args[1])
	if err != nil {
		log.Fatalf("Error while reading %s: %v\n", os.Args[1], err)
	}
	var kubeObj v1beta1.EventTaxonomy
	err = json.Unmarshal(file, &kubeObj)
	if err != nil {
		log.Fatalf("Error while parsing JSON from %s: %v\n", os.Args[1], err)
	}
	taxonomy := kubeObj.Spec.Taxonomy
	taxonomy.BuildIndex()
	// baseCols contains column names that we should not touch
	chSpec := kubeObj.Spec.Clickhouse
	for _, binding := range chSpec.TableBindings {
		colAdded := make(map[string]bool)
		table := binding.Table.DeepCopy()
		propertyColumns := controllers.EventPropertiesAsColumns(&taxonomy, binding)
		table.Columns = make([]*clickhouse.Column, 0, len(binding.Table.Columns)+len(chSpec.BaseColumns)+len(propertyColumns))
		for _, col := range binding.Table.Columns {
			colAdded[col.Name] = true
			table.Columns = append(table.Columns, col)
		}
		for _, col := range chSpec.BaseColumns {
			if _, added := colAdded[col.Name]; added {
				continue
			}
			colAdded[col.Name] = true
			table.Columns = append(table.Columns, col.DeepCopy())
		}
		for _, col := range propertyColumns {
			if _, added := colAdded[col.Name]; added {
				continue
			}
			colAdded[col.Name] = true
			table.Columns = append(table.Columns, col)
		}
		fmt.Printf("CREATE DATABASE IF NOT EXISTS %s ON CLUSTER %v;\n", table.DatabaseName, table.ClusterName)
		fmt.Printf("%s;\n\n", clickhouse.MakeCreateTableQuery(table))
	}
}
