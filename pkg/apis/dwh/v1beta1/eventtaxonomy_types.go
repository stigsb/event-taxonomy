// Copyright 2018-2019 Zedge, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and

package v1beta1

import (
	"gitlab.com/zedge-oss/zeppa/event-taxonomy/pkg/clickhouse"
	"gitlab.com/zedge-oss/zeppa/event-taxonomy/pkg/model"

	"encoding/json"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EventTaxonomySpec defines the desired state of EventTaxonomy
type EventTaxonomySpec struct {
	Clickhouse   *ClickhouseSpec     `json:"clickhouse,omitempty"`
	Kafka        *KafkaSpec          `json:"kafka,omitempty"`
	Taxonomy     model.EventTaxonomy `json:"taxonomy"`
	TwinTaxonomy string              `json:"twinTaxonomy,omitempty"`
}

// KafkaTopicBinding maps a topic to a list of property and enum field lifecycles
type KafkaTopicBinding struct {
	Name       string            `json:"name"`
	Lifecycles []model.Lifecycle `json:"lifecycles,omitempty"`
}

// KafkaSpec defines kafka topic bindings for this taxonomy
type KafkaSpec struct {
	TopicBindings []KafkaTopicBinding `json:"topicBindings"`
}

// ClickhouseTableBinding describes a mapping from an event taxonomy to a Clickhouse table.
// Each binding typically includes event properties and enum fields with a certain set of
// lifecycles, to create separate tables for beta and production.
type ClickhouseTableBinding struct {
	Lifecycles []model.Lifecycle `json:"lifecycles"`
	// Deprecated: use Table.Database instead
	DatabaseName string `json:"databaseName,omitempty"`
	// Deprecated: use Table.Name instead
	TableName            string            `json:"tableName,omitempty"`
	Table                *clickhouse.Table `json:"table,omitempty"`
	ColumnTTLOverrides   map[string]string `json:"ttlOverrides,omitempty"`
	ColumnCodecOverrides map[string]string `json:"codecOverrides,omitempty"`
}

func (tb *ClickhouseTableBinding) UnmarshalJSON(b []byte) error {
	type plain ClickhouseTableBinding
	if err := json.Unmarshal(b, (*plain)(tb)); err != nil {
		return err
	}
	if tb.Table == nil {
		tb.Table = &clickhouse.Table{
			Name:         tb.TableName,
			DatabaseName: tb.DatabaseName,
			Columns:      []*clickhouse.Column{},
		}
		tb.TableName = ""
		tb.DatabaseName = ""
	}
	return nil
}

// ClickhouseSpec describes the Clickhouse table we're maintaining for this taxonomy
type ClickhouseSpec struct {
	// Server should be host:port
	DSN string `json:"dsn"`

	// ClusterName must be set to ensure DDL queries are run with `ON CLUSTER ...`.
	ClusterName string `json:"clusterName,omitempty"`

	// Columns that should be present in all tables for this EventTaxonomy.
	BaseColumns []clickhouse.Column `json:"baseColumns,omitempty"`

	// Deprecated: use BaseColumns instead
	MaterializedColumns []clickhouse.Column      `json:"materializedColumns,omitempty"`
	TableBindings       []ClickhouseTableBinding `json:"tableBindings"`
}

func (cs *ClickhouseSpec) UnmarshalJSON(data []byte) error {
	type plain ClickhouseSpec
	if err := json.Unmarshal(data, (*plain)(cs)); err != nil {
		return err
	}
	// We want to use only BaseColumns. For compatibility, if MaterializedColumns
	// is set and BaseColumns isn't, move it.
	if len(cs.BaseColumns) == 0 && len(cs.MaterializedColumns) > 0 {
		cs.BaseColumns = cs.MaterializedColumns
		cs.MaterializedColumns = make([]clickhouse.Column, 0)
	}
	// Apply "ON CLUSTER" defaults to table bindings
	for _, binding := range cs.TableBindings {
		if binding.Table != nil && binding.Table.ClusterName == "" {
			binding.Table.ClusterName = cs.ClusterName
		}
	}
	return nil
}

// EventTaxonomyStatus defines the observed state of EventTaxonomy
type EventTaxonomyStatus struct {
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
// +k8s:openapi-gen=true
// +genclient

// EventTaxonomy is the Schema for the eventtaxonomies API
type EventTaxonomy struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   EventTaxonomySpec   `json:"spec,omitempty"`
	Status EventTaxonomyStatus `json:"status,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// EventTaxonomyList contains a list of EventTaxonomy
type EventTaxonomyList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []EventTaxonomy `json:"items"`
}

func init() {
	SchemeBuilder.Register(&EventTaxonomy{}, &EventTaxonomyList{})
}
