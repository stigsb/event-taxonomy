// +build !ignore_autogenerated

// Copyright 2018-2019 Zedge, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Code generated by controller-gen. DO NOT EDIT.

package v1beta1

import (
	"gitlab.com/zedge-oss/zeppa/event-taxonomy/pkg/clickhouse"
	"gitlab.com/zedge-oss/zeppa/event-taxonomy/pkg/model"
	runtime "k8s.io/apimachinery/pkg/runtime"
)

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *ClickhouseSpec) DeepCopyInto(out *ClickhouseSpec) {
	*out = *in
	if in.BaseColumns != nil {
		in, out := &in.BaseColumns, &out.BaseColumns
		*out = make([]clickhouse.Column, len(*in))
		copy(*out, *in)
	}
	if in.MaterializedColumns != nil {
		in, out := &in.MaterializedColumns, &out.MaterializedColumns
		*out = make([]clickhouse.Column, len(*in))
		copy(*out, *in)
	}
	if in.TableBindings != nil {
		in, out := &in.TableBindings, &out.TableBindings
		*out = make([]ClickhouseTableBinding, len(*in))
		for i := range *in {
			(*in)[i].DeepCopyInto(&(*out)[i])
		}
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new ClickhouseSpec.
func (in *ClickhouseSpec) DeepCopy() *ClickhouseSpec {
	if in == nil {
		return nil
	}
	out := new(ClickhouseSpec)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *ClickhouseTableBinding) DeepCopyInto(out *ClickhouseTableBinding) {
	*out = *in
	if in.Lifecycles != nil {
		in, out := &in.Lifecycles, &out.Lifecycles
		*out = make([]model.Lifecycle, len(*in))
		copy(*out, *in)
	}
	if in.Table != nil {
		in, out := &in.Table, &out.Table
		*out = new(clickhouse.Table)
		(*in).DeepCopyInto(*out)
	}
	if in.ColumnTTLOverrides != nil {
		in, out := &in.ColumnTTLOverrides, &out.ColumnTTLOverrides
		*out = make(map[string]string, len(*in))
		for key, val := range *in {
			(*out)[key] = val
		}
	}
	if in.ColumnCodecOverrides != nil {
		in, out := &in.ColumnCodecOverrides, &out.ColumnCodecOverrides
		*out = make(map[string]string, len(*in))
		for key, val := range *in {
			(*out)[key] = val
		}
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new ClickhouseTableBinding.
func (in *ClickhouseTableBinding) DeepCopy() *ClickhouseTableBinding {
	if in == nil {
		return nil
	}
	out := new(ClickhouseTableBinding)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *EventTaxonomy) DeepCopyInto(out *EventTaxonomy) {
	*out = *in
	out.TypeMeta = in.TypeMeta
	in.ObjectMeta.DeepCopyInto(&out.ObjectMeta)
	in.Spec.DeepCopyInto(&out.Spec)
	out.Status = in.Status
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new EventTaxonomy.
func (in *EventTaxonomy) DeepCopy() *EventTaxonomy {
	if in == nil {
		return nil
	}
	out := new(EventTaxonomy)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyObject is an autogenerated deepcopy function, copying the receiver, creating a new runtime.Object.
func (in *EventTaxonomy) DeepCopyObject() runtime.Object {
	if c := in.DeepCopy(); c != nil {
		return c
	}
	return nil
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *EventTaxonomyList) DeepCopyInto(out *EventTaxonomyList) {
	*out = *in
	out.TypeMeta = in.TypeMeta
	out.ListMeta = in.ListMeta
	if in.Items != nil {
		in, out := &in.Items, &out.Items
		*out = make([]EventTaxonomy, len(*in))
		for i := range *in {
			(*in)[i].DeepCopyInto(&(*out)[i])
		}
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new EventTaxonomyList.
func (in *EventTaxonomyList) DeepCopy() *EventTaxonomyList {
	if in == nil {
		return nil
	}
	out := new(EventTaxonomyList)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyObject is an autogenerated deepcopy function, copying the receiver, creating a new runtime.Object.
func (in *EventTaxonomyList) DeepCopyObject() runtime.Object {
	if c := in.DeepCopy(); c != nil {
		return c
	}
	return nil
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *EventTaxonomySpec) DeepCopyInto(out *EventTaxonomySpec) {
	*out = *in
	if in.Clickhouse != nil {
		in, out := &in.Clickhouse, &out.Clickhouse
		*out = new(ClickhouseSpec)
		(*in).DeepCopyInto(*out)
	}
	if in.Kafka != nil {
		in, out := &in.Kafka, &out.Kafka
		*out = new(KafkaSpec)
		(*in).DeepCopyInto(*out)
	}
	in.Taxonomy.DeepCopyInto(&out.Taxonomy)
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new EventTaxonomySpec.
func (in *EventTaxonomySpec) DeepCopy() *EventTaxonomySpec {
	if in == nil {
		return nil
	}
	out := new(EventTaxonomySpec)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *EventTaxonomyStatus) DeepCopyInto(out *EventTaxonomyStatus) {
	*out = *in
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new EventTaxonomyStatus.
func (in *EventTaxonomyStatus) DeepCopy() *EventTaxonomyStatus {
	if in == nil {
		return nil
	}
	out := new(EventTaxonomyStatus)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *KafkaSpec) DeepCopyInto(out *KafkaSpec) {
	*out = *in
	if in.TopicBindings != nil {
		in, out := &in.TopicBindings, &out.TopicBindings
		*out = make([]KafkaTopicBinding, len(*in))
		for i := range *in {
			(*in)[i].DeepCopyInto(&(*out)[i])
		}
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new KafkaSpec.
func (in *KafkaSpec) DeepCopy() *KafkaSpec {
	if in == nil {
		return nil
	}
	out := new(KafkaSpec)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *KafkaTopicBinding) DeepCopyInto(out *KafkaTopicBinding) {
	*out = *in
	if in.Lifecycles != nil {
		in, out := &in.Lifecycles, &out.Lifecycles
		*out = make([]model.Lifecycle, len(*in))
		copy(*out, *in)
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new KafkaTopicBinding.
func (in *KafkaTopicBinding) DeepCopy() *KafkaTopicBinding {
	if in == nil {
		return nil
	}
	out := new(KafkaTopicBinding)
	in.DeepCopyInto(out)
	return out
}
