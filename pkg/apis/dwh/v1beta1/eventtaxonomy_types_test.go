/*
Copyright 2018 Zedge, Inc..

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1beta1

import (
	"encoding/json"
	"testing"

	Ω "github.com/onsi/gomega"
)

func TestUnmarshallingCompatibility(t *testing.T) {
	chSpecJson := []byte(`{
	  "dsn": "",
	  "materializedColumns": [
	    {
	      "name": "date",
	      "type": "DateTime"
	    }
	  ]
	}`)
	g := Ω.NewGomegaWithT(t)
	var chSpec ClickhouseSpec
	g.Expect(json.Unmarshal(chSpecJson, &chSpec)).To(Ω.Succeed())
	g.Expect(chSpec.MaterializedColumns).To(Ω.BeEmpty())
	g.Expect(chSpec.BaseColumns).To(Ω.HaveLen(1))
	g.Expect(chSpec.BaseColumns[0].Name).To(Ω.Equal("date"))
	g.Expect(chSpec.BaseColumns[0].Type).To(Ω.Equal("DateTime"))
	chTBJson := `{"databaseName": "database", "tableName": "table"}`
	var chTB ClickhouseTableBinding
	g.Expect(json.Unmarshal([]byte(chTBJson), &chTB)).To(Ω.Succeed())
	table := chTB.Table
	g.Expect(table).ToNot(Ω.BeNil())
	g.Expect(table.DatabaseName).To(Ω.Equal("database"))
	g.Expect(table.Name).To(Ω.Equal("table"))
}

func TestDefaultingClusterNameToTables(t *testing.T) {
	chSpecJson := []byte(`{
	  "clusterName": "mycluster",
	  "tableBindings": [{"table": {}}]
	}`)
	g := Ω.NewGomegaWithT(t)
	var chSpec ClickhouseSpec
	g.Expect(json.Unmarshal(chSpecJson, &chSpec)).To(Ω.Succeed())
	g.Expect(chSpec.TableBindings[0].Table.ClusterName).To(Ω.Equal("mycluster"))
}
