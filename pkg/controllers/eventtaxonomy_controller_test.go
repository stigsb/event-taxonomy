/*
Copyright 2018 Zedge, Inc..

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"testing"
	"time"

	. "github.com/onsi/gomega"
	"github.com/stretchr/testify/assert"
	"k8s.io/api/core/v1"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"

	dwh "gitlab.com/zedge-oss/zeppa/event-taxonomy/pkg/apis/dwh/v1beta1"
	"gitlab.com/zedge-oss/zeppa/event-taxonomy/pkg/clickhouse"
	"gitlab.com/zedge-oss/zeppa/event-taxonomy/pkg/model"
)

var c client.Client

const timeout = time.Second * 60
const pollingInterval = time.Millisecond * 100

var expectedRequest = reconcile.Request{NamespacedName: types.NamespacedName{Name: "foo", Namespace: "default"}}
var expectedRequest2 = reconcile.Request{NamespacedName: types.NamespacedName{Name: "bar", Namespace: "default"}}
var taxCmName = types.NamespacedName{Name: "foo-event-taxonomy", Namespace: "default"}
var taxCmNameBar = types.NamespacedName{Name: "bar-event-taxonomy", Namespace: "default"}
var fatTaxCmName = types.NamespacedName{Name: "event-taxonomies", Namespace: "default"}
var lastModToday metav1.Time

func init() {
	lastModToday = metav1.Time{Time: time.Now()}
}

func TestReconcile(t *testing.T) {
	g := NewGomegaWithT(t)
	timeTime, err := time.Parse(time.RFC3339, "2018-01-02T15:04:05Z")
	lastMod := metav1.NewTime(timeTime)
	g.Expect(err).NotTo(HaveOccurred())
	instance := &dwh.EventTaxonomy{
		ObjectMeta: metav1.ObjectMeta{Name: "foo", Namespace: "default"},
		Spec: dwh.EventTaxonomySpec{
			Taxonomy: model.EventTaxonomy{
				AppID: "foo",
				Enums: []*model.EventEnum{
					{
						Name: "Event",
						Fields: []*model.EventEnumField{
							{Name: "A", Value: 1, Lifecycle: model.ProductionLifecycle, LastModified: lastMod},
						},
					},
				},
				Properties: []*model.EventProperty{
					{
						Name:         "event",
						Type:         "Enum:Event",
						Lifecycle:    model.ProductionLifecycle,
						Scope:        model.EnvelopeScope,
						LastModified: lastModToday,
					},
				},
			},
		},
	}

	instance2 := &dwh.EventTaxonomy{
		ObjectMeta: metav1.ObjectMeta{Name: "bar", Namespace: "default"},
		Spec: dwh.EventTaxonomySpec{
			Taxonomy: model.EventTaxonomy{
				AppID: "bao",
				Enums: []*model.EventEnum{
					&model.EventEnum{
						Name: "Event",
						Fields: []*model.EventEnumField{
							&model.EventEnumField{Name: "A", Value: 1, Lifecycle: model.ProductionLifecycle, LastModified: lastMod},
						},
					},
				},
				Properties: []*model.EventProperty{
					&model.EventProperty{
						Name:         "event",
						Type:         "Enum:Event",
						Lifecycle:    model.ProductionLifecycle,
						Scope:        model.EnvelopeScope,
						LastModified: lastModToday,
					},
				},
			},
		},
	}

	// Setup the Manager and Controller.  Wrap the Controller Reconcile function so it writes each request to a
	// channel when it is finished.
	mgr, err := manager.New(cfg, manager.Options{})
	g.Expect(err).NotTo(HaveOccurred())
	c = mgr.GetClient()

	reconciler := &EventTaxonomyReconciler{
		Client:     mgr.GetClient(),
		Scheme:     mgr.GetScheme(),
		Log:        logf.Log,
		StatefulDB: clickhouse.NewLoggingStatefulDB(),
	}
	g.Expect(reconciler.SetupWithManager(mgr)).To(Succeed())
	ctx, err := SetupEventTaxonomyReconcilerTest()
	g.Expect(err).NotTo(HaveOccurred())

	stopMgr, mgrStopped := StartTestManager(mgr, g)

	defer func() {
		close(stopMgr)
		mgrStopped.Wait()
	}()

	// Create the EventTaxonomy object and expect the Reconcile and ConfigMap to be created
	err = c.Create(ctx, instance)
	// The instance object may not be a valid object because it might be missing some required fields.
	// Please modify the instance object by adding required fields and then remove the following if statement.
	if apierrors.IsInvalid(err) {
		t.Logf("failed to create object, got an invalid object error: %v", err)
		return
	}
	g.Expect(err).NotTo(HaveOccurred())
	defer c.Delete(ctx, instance)
	//g.Eventually(requests, timeout).Should(Receive(Equal(expectedRequest)))

	// Create second EventTaxonomy object and expect the Reconcile and ConfigMap to be created
	err = c.Create(ctx, instance2)
	// The instance object may not be a valid object because it might be missing some required fields.
	// Please modify the instance object by adding required fields and then remove the following if statement.
	if apierrors.IsInvalid(err) {
		t.Logf("failed to create object, got an invalid object error: %v", err)
		return
	}
	g.Expect(err).NotTo(HaveOccurred())
	defer c.Delete(ctx, instance2)

	configMapFoo := &v1.ConfigMap{}
	g.Eventually(func() error { return c.Get(ctx, taxCmName, configMapFoo) }, timeout).
		Should(Succeed())

	configMapBar := &v1.ConfigMap{}
	g.Eventually(func() error { return c.Get(ctx, taxCmNameBar, configMapBar) }, timeout).
		Should(Succeed())

	fatConfigMap := &v1.ConfigMap{}
	g.Eventually(func() error { return c.Get(ctx, fatTaxCmName, fatConfigMap) }, timeout).
		Should(Succeed())

	// Delete the Deployment and expect Reconcile to be called for Deployment deletion
	g.Expect(c.Delete(ctx, configMapFoo)).NotTo(HaveOccurred())
	g.Expect(c.Delete(ctx, configMapBar)).NotTo(HaveOccurred())
	g.Expect(c.Delete(ctx, fatConfigMap)).NotTo(HaveOccurred())
	g.Eventually(
		func() error {
			return c.Get(ctx, taxCmName, configMapFoo)
		},
		timeout,
		pollingInterval,
	).Should(Succeed())
	g.Eventually(
		func() error {
			return c.Get(ctx, taxCmNameBar, configMapBar)
		},
		timeout,
		pollingInterval,
	).Should(Succeed())
	g.Eventually(
		func() error {
			return c.Get(ctx, fatTaxCmName, fatConfigMap)
		},
		timeout,
		pollingInterval,
	).Should(Succeed())

	// Manually delete ConfigMap since GC isn't enabled in the test control plane
	c.Delete(ctx, configMapFoo)
	c.Delete(ctx, configMapBar)
	c.Delete(ctx, fatConfigMap)
}

func TestEventPropertiesAsColumns(t *testing.T) {
	// EventPropertiesAsColumns(taxonomy *model.EventTaxonomy, lifecycles []model.Lifecycle) []*Column
	tax := &model.EventTaxonomy{
		Properties: []*model.EventProperty{
			{Type: "DateTime", Name: "date", Scope: model.InternalScope, Lifecycle: model.ProductionLifecycle},
			{Type: "Int8", Name: "foo", Scope: model.EventScope, Lifecycle: model.ProductionLifecycle},
			{Type: "Int8", Name: "bar", Scope: model.EventScope, Lifecycle: model.PreProductionLifecycle},
		},
	}
	preProdBinding := dwh.ClickhouseTableBinding{
		Lifecycles: []model.Lifecycle{model.ProductionLifecycle, model.PreProductionLifecycle},
		ColumnTTLOverrides: map[string]string{
			"foo": "date + INTERVAL 1 MONTH",
		},
		ColumnCodecOverrides: map[string]string{
			"bar": "CODEC(LZ4)",
		},
	}
	prodBinding := dwh.ClickhouseTableBinding{
		Lifecycles: []model.Lifecycle{model.ProductionLifecycle},
		ColumnTTLOverrides: map[string]string{
			"foo": "date + INTERVAL 1 MONTH",
		},
		ColumnCodecOverrides: map[string]string{
			"bar": "CODEC(LZ4)",
		},
	}
	expectedDateCol := clickhouse.Column{
		Name: "date",
		Type: "DateTime",
	}
	expectedFooCol := clickhouse.Column{
		Name: "foo",
		Type: "Int8",
		TTL:  "date + INTERVAL 1 MONTH",
	}
	expectedBarCol := clickhouse.Column{
		Name:  "bar",
		Type:  "Int8",
		Codec: "CODEC(LZ4)",
	}
	cols := EventPropertiesAsColumns(tax, preProdBinding)
	assert.Len(t, cols, 3)
	assert.Equal(t, expectedDateCol, *cols[0])
	assert.Equal(t, expectedFooCol, *cols[1])
	assert.Equal(t, expectedBarCol, *cols[2])
	cols = EventPropertiesAsColumns(tax, prodBinding)
	assert.Len(t, cols, 2)
	assert.Equal(t, expectedDateCol, *cols[0])
	assert.Equal(t, expectedFooCol, *cols[1])
}

func TestSortDistributedFirst(t *testing.T) {
	androidEventsLocal := clickhouse.Table{
		Engine:       "MergeTree",
		DatabaseName: "android",
		Name:         "events_local",
	}
	androidEvents := clickhouse.Table{
		Engine:       "Distributed",
		DatabaseName: "android",
		Name:         "events",
	}
	androidPreprod := clickhouse.Table{
		Engine:       "Distributed",
		DatabaseName: "android",
		Name:         "preprod_events",
	}
	var tables = []dwh.ClickhouseTableBinding{
		{Table: &androidEventsLocal},
		{Table: &androidEvents},
		{Table: &androidPreprod},
	}
	sortDistributedFirst(tables)
	assert.Equal(t, tables[2].Table.Name, "events_local")
}
