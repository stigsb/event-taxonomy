// Copyright 2018-2019 Zedge, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and

package clickhouse

import (
	"fmt"
	"strings"
	"testing"

	"github.com/ory/dockertest"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestCreateTable(t *testing.T) {
	table := Table{
		DatabaseName: "default",
		Name:         "foo",
		ClusterName:  "logs",
		Engine:       "Log",
		Columns: []*Column{
			{Name: "id", Type: "Int64"},
			{Name: "name", Type: "String"},
			{Name: "nhash", Type: "Int64", DefaultKind: "MATERIALIZED", DefaultExpression: "CAST(sipHash64(name), 'Int64')"},
			{Name: "num", Type: "Int8", DefaultKind: "DEFAULT", DefaultExpression: "CAST(42, 'Int8')"},
		},
	}
	expected := "CREATE TABLE default.foo ON CLUSTER logs (\n" +
		"  id Int64,\n" +
		"  name String,\n" +
		"  nhash Int64 MATERIALIZED CAST(sipHash64(name), 'Int64'),\n" +
		"  num Int8 DEFAULT CAST(42, 'Int8')\n" +
		") ENGINE = Log"
	assert.Equal(t, expected, MakeCreateTableQuery(&table))
	assert.Equal(t, expected, table.String())
}

func TestTable_AddAndModifyColumn(t *testing.T) {
	nameHashCol := &Column{Name: "name_hash", Type: "Int64", DefaultKind: "MATERIALIZED", DefaultExpression: "sipHash64(name)"}
	//nameCol := &Column{Name: "name", Type: "String", TTL: "date + INTERVAL 1 DAY"}
	//numCol := &Column{Name: "num", Type: "Int8", DefaultKind: "DEFAULT", DefaultExpression: "42"}
	createTableQuery := strings.ReplaceAll(`CREATE TABLE default.foo (
	  id Int64,
	  date DateTime,
	  name String
	) Engine=MergeTree()
	  PARTITION BY toYYYYMM(date)
	  ORDER BY (date)
	`, "\n", "")
	runWithClickhouseContainer(t, func(docker *dockertest.Pool, container *dockertest.Resource, db *StatefulDB) {
		_, err := db.Exec(createTableQuery)
		require.NoError(t, err)
		inspector := NewTableInspector(db.DB)
		table, err := inspector.Inspect("default", "foo")
		require.NoError(t, err)
		assert.Len(t, table.Columns, 3)
		assert.NoError(t, table.AddColumn(nameHashCol, db))
		inspCol, err := table.InspectColumn(nameHashCol.Name, db)
		assert.NoError(t, err)
		assert.Equal(t, `CAST(sipHash64(name), 'Int64')`, inspCol.DefaultExpression, "expression cache: %v", db.expressionCache)
		assert.False(t, nameHashCol.Equals(*inspCol))
		assert.Truef(t, nameHashCol.EquivalentTo(*inspCol, db), "expression cache: %v", db.expressionCache)
		nameHashCol.Type = "UInt64"
		assert.NoError(t, table.ModifyColumn(nameHashCol, db))
		inspCol, err = table.InspectColumn(nameHashCol.Name, db)
		assert.NoError(t, err)
		assert.True(t, nameHashCol.EquivalentTo(*inspCol, db))
	})
}

func TestColumn_EquivalentTo(t *testing.T) {
	type testCase struct {
		a          Column
		b          Column
		equivalent bool
	}
	db := NewStatefulDB()
	db.LearnExpression("date + INTERVAL 1 DAY", "date + toIntervalDay(1)")
	for i, tc := range []testCase{
		{
			a:          Column{Type: "Int8", DefaultKind: "DEFAULT", DefaultExpression: "0"},
			b:          Column{Type: "Int8"},
			equivalent: true,
		},
		{
			a:          Column{Type: "UInt8", DefaultKind: "DEFAULT", DefaultExpression: "0"},
			b:          Column{Type: "UInt8"},
			equivalent: true,
		},
		{
			a:          Column{Type: "Float32", DefaultKind: "DEFAULT", DefaultExpression: "0"},
			b:          Column{Type: "Float32"},
			equivalent: true,
		},
		{
			a:          Column{Type: "Int16"},
			b:          Column{Type: "Int32"},
			equivalent: false,
		},
		{
			a:          Column{Type: "Int32", DefaultKind: "ALIAS"},
			b:          Column{Type: "Int32"},
			equivalent: false,
		},
		{
			a:          Column{Type: "Int32", DefaultKind: "DEFAULT", DefaultExpression: "-1"},
			b:          Column{Type: "Int32"},
			equivalent: false,
		},
		{
			a:          Column{Type: "Nullable(Int32)", DefaultKind: "DEFAULT", DefaultExpression: "NULL"},
			b:          Column{Type: "Nullable(Int32)"},
			equivalent: true,
		},
		{
			a:          Column{Type: "Array(Int32)", DefaultKind: "DEFAULT", DefaultExpression: "[]"},
			b:          Column{Type: "Array(Int32)"},
			equivalent: true,
		},
		{
			a:          Column{Type: "String", DefaultKind: "DEFAULT", DefaultExpression: "''"},
			b:          Column{Type: "String"},
			equivalent: true,
		},
		{
			a:          Column{Type: "FixedString(8)", DefaultKind: "DEFAULT", DefaultExpression: "''"},
			b:          Column{Type: "FixedString(8)"},
			equivalent: true,
		},
		{
			a:          Column{Type: "String", Codec: "LZ4"},
			b:          Column{Type: "String", Codec: "CODEC(LZ4)"},
			equivalent: true,
		},
		{
			a:          Column{Type: "String", TTL: "date + INTERVAL 1 DAY"},
			b:          Column{Type: "String", TTL: "date + toIntervalDay(1)"},
			equivalent: true,
		},
	} {
		t.Run(fmt.Sprintf("%d", i), func(t *testing.T) {
			assert.Equal(t, tc.equivalent, tc.a.EquivalentTo(tc.b, db))
		})
	}
}
