// Copyright 2018-2019 Zedge, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and

package clickhouse

import (
	"database/sql"
	"fmt"
)

// TableInspector :
type TableInspector struct {
	db *sql.DB
}

// NewTableInspector creates a new TableReflector object
func NewTableInspector(db *sql.DB) *TableInspector {
	r := &TableInspector{db: db}
	return r
}

// TableExists tells whether a given table exists
func (i TableInspector) TableExists(database, table string) (bool, error) {
	rows, err := i.db.Query("SELECT COUNT() FROM system.tables WHERE database = ? AND name = ?", database, table)
	if err != nil {
		return false, err
	}
	rows.Next()
	count := 0
	err = rows.Scan(&count)
	if err != nil {
		return false, err
	}
	return count == 1, nil
}

func readColumnFromDescRows(rows *sql.Rows, rowCols []string) (*Column, error) {
	var col Column
	// All of this column name trickery is necessary to be compatible with
	// different Clickhouse versions, because the output of "DESC" varies a bit,
	// and it doesn't allow you to pick columns. Also, the "system.columns" table
	// lacks column ttl information as of 19.13.
	colMap := map[string]*string{
		"name":               &col.Name,
		"type":               &col.Type,
		"default_type":       &col.DefaultKind,
		"default_expression": &col.DefaultExpression,
		"comment":            &col.Comment,
		"codec_expression":   &col.Codec,
		"ttl_expression":     &col.TTL,
	}
	destCols := make([]interface{}, 0)
	for _, colName := range rowCols {
		if destCol, found := colMap[colName]; found {
			destCols = append(destCols, destCol)
		}
	}
	err := rows.Scan(destCols...)
	if err != nil {
		return nil, fmt.Errorf("rows.Scan(): %v", err)
	}
	return &col, nil
}

// InspectInto populates `table` with the current table structure, or returns an error
func (i TableInspector) InspectInto(table *Table) error {
	rows, err := i.db.Query("DESCRIBE TABLE " + table.DatabaseName + "." + table.Name)
	if err != nil {
		return fmt.Errorf("db.Query(): %v", err)
	}
	defer func() { _ = rows.Close() }()
	table.Columns = []*Column{}
	rowCols, err := rows.Columns()
	if err != nil {
		return fmt.Errorf("rows.Columns(): %v", err)
	}
	for rows.Next() {
		col, err := readColumnFromDescRows(rows, rowCols)
		if err != nil {
			return err
		}
		table.Columns = append(table.Columns, col)
	}
	if err := rows.Err(); err != nil {
		return fmt.Errorf("rows.Err(): %v", err)
	}
	return nil
}

func (i TableInspector) Inspect(databaseName, tableName string) (*Table, error) {
	table := Table{DatabaseName: databaseName, Name: tableName}
	if err := i.InspectInto(&table); err != nil {
		return nil, err
	}
	return &table, nil
}
