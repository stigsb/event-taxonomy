// Copyright 2018-2019 Zedge, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and

package clickhouse

import (
	"fmt"
	"strings"

	logf "sigs.k8s.io/controller-runtime/pkg/runtime/log"
)

var log = logf.Log.WithName("event-taxonomy-controller").WithName("clickhouse")

// Table describes a Clickhouse table. Supports only TABLE, not VIEW or MATERIALIZED VIEW.
// +k8s:deepcopy-gen=true
type Table struct {
	Name         string            `json:"name"`
	DatabaseName string            `json:"databaseName"`
	ClusterName  string            `json:"clusterName,omitempty"`
	Engine       string            `json:"engine"`
	PartitionBy  string            `json:"partitionBy,omitempty"`
	PrimaryKey   string            `json:"primaryKey,omitempty"`
	OrderBy      string            `json:"orderBy,omitempty"`
	SampleBy     string            `json:"sampleBy,omitempty"`
	TTL          string            `json:"ttl,omitempty"`
	Settings     map[string]string `json:"settings,omitempty"`
	Columns      []*Column         `json:"columns,omitempty"`
}

// Column describes a Clickhouse table column
// +k8s:deepcopy-gen=true
type Column struct {
	Name string `json:"name"`
	Type string `json:"type"`
	// Empty string, "DEFAULT", "ALIAS" or "MATERIALIZED"
	DefaultKind       string `json:"defaultKind,omitempty"`
	DefaultExpression string `json:"defaultExpression,omitempty"`
	TTL               string `json:"ttl,omitempty"`
	Codec             string `json:"codec,omitempty"`
	Comment           string `json:"comment,omitempty"`
}

func (c Column) hasExpression() bool {
	return c.TTL != "" || c.DefaultKind != ""
}

func (c Column) createOrAlterString(db *StatefulDB) string {
	str := c.Name + " " + c.Type
	if c.DefaultKind != "" && c.DefaultExpression != "" {
		str += " " + c.DefaultKind + " " + db.NormalizeExpression(c.DefaultExpression)
	}
	if c.Codec != "" {
		str += " " + normalizeCodec(c.Codec)
	}
	if c.TTL != "" {
		str += " TTL " + db.NormalizeExpression(c.TTL)
	}
	return str
}

// normalizeCodec tries to normalize the codec expression the same way Clickhouse does.
func normalizeCodec(codec string) string {
	if codec == "" {
		return ""
	}
	if strings.HasPrefix(strings.ToUpper(codec), "CODEC(") && strings.HasSuffix(codec, ")") {
		return "CODEC(" + strings.ToUpper(codec[6:])
	}
	return "CODEC(" + codec + ")"
}

// SameAs compares two columns (assumed to have the same Name) and returns
// whether they are equal.
func (c Column) Equals(other Column) bool {
	return c.Type == other.Type &&
		c.TTL == other.TTL &&
		strings.ToUpper(c.DefaultKind) == strings.ToUpper(other.DefaultKind) &&
		c.DefaultExpression == other.DefaultExpression &&
		normalizeCodec(c.Codec) == normalizeCodec(other.Codec)
}

func isNumericType(t string) bool {
	return strings.HasPrefix(t, "Int") || strings.HasPrefix(t, "UInt") || strings.HasPrefix(t, "Float")
}

func isStringType(t string) bool {
	return t == "String" || strings.HasPrefix(t, "FixedString(")
}

func isNullableType(t string) bool {
	return strings.HasPrefix(t, "Nullable(")
}

func isArrayType(t string) bool {
	return strings.HasPrefix(t, "Array(")
}

func normalizeKindAndExpression(col Column) (string, string) {
	kind := strings.ToUpper(col.DefaultKind)
	expr := col.DefaultExpression
	if kind == "" || kind == "DEFAULT" {
		// All of these defaults are equivalent to not specifying one
		if (isNumericType(col.Type) && expr == "0") ||
			(isStringType(col.Type) && expr == "''") ||
			(isNullableType(col.Type) && strings.ToUpper(expr) == "NULL") ||
			(isArrayType(col.Type) && expr == "[]") {
			return "", ""
		}
	}
	return kind, expr
}

// EquivalentTo compares two columns (assumed to have the same Name) and returns
// whether they are equal after applying some equality normalizations.
// For example, a non-nullable Int32 column without a default, and one with
// DEFAULT 0 will be treated as equal.
func (c Column) EquivalentTo(other Column, db *StatefulDB) bool {
	myKind, myExpr := normalizeKindAndExpression(c)
	otherKind, otherExpr := normalizeKindAndExpression(other)
	return c.Type == other.Type &&
		db.NormalizeExpression(c.TTL) == db.NormalizeExpression(other.TTL) &&
		myKind == otherKind &&
		db.NormalizeExpression(myExpr) == db.NormalizeExpression(otherExpr) &&
		normalizeCodec(c.Codec) == normalizeCodec(other.Codec)
}

func (t *Table) String() string {
	return MakeCreateTableQuery(t)
}

func (t *Table) alter(db *StatefulDB, querySnippet string) error {
	query := fmt.Sprintf("ALTER TABLE %s.%s ", t.DatabaseName, t.Name)
	if len(t.ClusterName) > 0 {
		query += "ON CLUSTER " + t.ClusterName + " "
	}
	query += querySnippet
	log.V(1).Info("Running query", "query", query)
	_, err := db.Exec(query)
	if err != nil {
		log.Error(err, "failed running ALTER TABLE", "query", query)
	}
	return err
}

// AddColumn adds a column to the table through the `db` connection
func (t *Table) AddColumn(col *Column, db *StatefulDB) error {
	if err := t.alter(db, "ADD COLUMN "+col.createOrAlterString(db)); err != nil {
		return err
	}
	if err := db.MaybeObserveColumnNormalization(t, col); err != nil {
		return err
	}
	t.Columns = append(t.Columns, col.DeepCopy())
	return nil
}

// ModifyColumn changes the type of a column through the `db` connection
func (t *Table) ModifyColumn(col *Column, db *StatefulDB) error {
	if err := t.alter(db, "MODIFY COLUMN "+col.createOrAlterString(db)); err != nil {
		return err
	}
	if err := db.MaybeObserveColumnNormalization(t, col); err != nil {
		return err
	}
	// this is obviously not concurrency-safe, if that is needed add a mutex to Table
	if i := t.indexOfColumn(col.Name); i != -1 {
		t.Columns[i] = col
	}
	return nil
}

// DropColumn drops a column from the table through the `db` connection
func (t *Table) DropColumn(col *Column, db *StatefulDB) error {
	err := t.alter(db, "DROP COLUMN "+col.Name)
	if err == nil {
		if i := t.indexOfColumn(col.Name); i != -1 {
			copy(t.Columns[i:], t.Columns[i+1:])
			t.Columns[len(t.Columns)-1] = nil
			t.Columns = t.Columns[:len(t.Columns)-1]
		}
	}
	return err
}

func (t *Table) indexOfColumn(colName string) int {
	for i, c := range t.Columns {
		if c.Name == colName {
			return i
		}
	}
	return -1
}

// HasColumn tells whether this table has a column with a given name (only looking at its state, does not query ClickHouse)
func (t *Table) HasColumn(colName string) bool {
	return t.indexOfColumn(colName) != -1
}

// GetColumn returns a column with a given name (only looking at its state, does not query ClickHouse)
func (t *Table) GetColumn(colName string) *Column {
	i := t.indexOfColumn(colName)
	if i == -1 {
		return nil
	}
	return t.Columns[i]
}

// InspectColumnOfTable populates `column` with the current structure of `column.Name` in `table`, or returns an error
func (t Table) InspectColumn(columnName string, db *StatefulDB) (*Column, error) {
	rows, err := db.Query("DESCRIBE TABLE " + t.DatabaseName + "." + t.Name)
	if err != nil {
		return nil, fmt.Errorf("db.Query(): %v", err)
	}
	defer func() { _ = rows.Close() }()
	rowCols, err := rows.Columns()
	if err != nil {
		return nil, fmt.Errorf("rows.Columns(): %v", err)
	}
	for rows.Next() {
		col, err := readColumnFromDescRows(rows, rowCols)
		if err != nil {
			return nil, err
		}
		if col.Name == columnName {
			return col, nil
		}
	}
	if err := rows.Err(); err != nil {
		return nil, fmt.Errorf("rows.Err(): %v", err)
	}
	return nil, nil
}

func (t Table) Exists(db *StatefulDB) (bool, error) {
	rows, err := db.Query(`SELECT COUNT() FROM system.tables WHERE database = ? AND name = ?`, t.DatabaseName, t.Name)
	if err != nil {
		return false, err
	}
	rows.Next()
	var count int
	err = rows.Scan(&count)
	if err != nil {
		return false, err
	}
	return count == 1, nil
}

// Inspect returns a new Table struct populated with the current structure of the table, or an error
func (t Table) Inspect(db *StatefulDB) (*Table, error) {
	table := &Table{Name: t.Name, DatabaseName: t.DatabaseName, ClusterName: t.ClusterName}
	rows, err := db.Query("DESCRIBE TABLE " + t.DatabaseName + "." + t.Name)
	if err != nil {
		return nil, fmt.Errorf("db.Query(): %v", err)
	}
	defer func() { _ = rows.Close() }()
	table.Columns = make([]*Column, 0)
	rowCols, err := rows.Columns()
	if err != nil {
		return nil, fmt.Errorf("rows.Columns(): %v", err)
	}
	for rows.Next() {
		col, err := readColumnFromDescRows(rows, rowCols)
		if err != nil {
			return nil, err
		}
		table.Columns = append(table.Columns, col)
	}
	if err := rows.Err(); err != nil {
		return nil, fmt.Errorf("rows.Err(): %v", err)
	}
	return table, nil
}

// MakeCreateTableQuery constructs an SQL statement for creating a table
func MakeCreateTableQuery(table *Table) string {
	query := "CREATE TABLE " + table.DatabaseName + "." + table.Name
	if table.ClusterName != "" {
		query += " ON CLUSTER " + table.ClusterName
	}
	query += " ("
	for i, column := range table.Columns {
		if i > 0 {
			query += ","
		}
		query += "\n  " + column.Name + " " + column.Type
		if column.DefaultKind != "" {
			query += " " + column.DefaultKind
		}
		if column.DefaultExpression != "" {
			query += " " + column.DefaultExpression
		}
	}
	query += "\n) ENGINE = " + table.Engine
	// ENGINE = ReplacingMergeTree()
	// PARTITION BY toYYYYMM(server_date)
	// ORDER BY (reinterpretAsUInt32(bin_zid), client_timestamp, event_dedupe_key)
	// SAMPLE BY reinterpretAsUInt32(bin_zid)
	// SETTINGS index_granularity = 8192
	if table.PartitionBy != "" {
		query += "\nPARTITION BY " + table.PartitionBy
	}
	if table.OrderBy != "" {
		query += "\nORDER BY " + table.OrderBy
	}
	if table.SampleBy != "" {
		query += "\nSAMPLE BY " + table.SampleBy
	}
	if table.TTL != "" {
		query += "\nTTL " + table.TTL
	}
	if len(table.Settings) > 0 {
		query += "\nSETTINGS "
		first := true
		for setting, value := range table.Settings {
			if first {
				first = false
			} else {
				query += ", "
			}
			query += setting + " = " + value
		}
	}
	return query
}
