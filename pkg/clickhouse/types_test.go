// Copyright 2018-2019 Zedge, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and

package clickhouse

import (
	"testing"

	"github.com/stretchr/testify/assert"

	. "gitlab.com/zedge-oss/zeppa/event-taxonomy/pkg/model"
)

func TestAsClickhouseType(t *testing.T) {
	tax := &EventTaxonomy{
		Enums: []*EventEnum{
			{
				Name: "Foo",
				Fields: []*EventEnumField{
					{Name: "A", Value: 1, Lifecycle: ProductionLifecycle},
					{Name: "C", Value: 3, Lifecycle: PreProductionLifecycle},
					{Name: "B", Value: 2, Lifecycle: ProductionEndOfLifeLifecycle},
				},
			},
			{
				Name: "WithZeroValue",
				Fields: []*EventEnumField{
					{Name: "Zero", Value: 0, Lifecycle: ProductionLifecycle},
					{Name: "One", Value: 1, Lifecycle: ProductionLifecycle},
				},
			},
			{
				Name: "Big",
				Fields: []*EventEnumField{
					{Name: "A", Value: 32767, Lifecycle: ProductionLifecycle},
				},
			},
		},
	}
	tax.BuildIndex()
	assertions := map[string]string{
		"Bool":                      "UInt8",
		"Array(Bool)":               "Array(UInt8)",
		"Enum:Foo":                  "Enum8('' = 0, 'A' = 1, 'B' = 2, 'C' = 3)",
		"Enum:Big":                  "Enum16('' = 0, 'A' = 32767)",
		"Array(Enum:Foo)":           "Array(Enum8('' = 0, 'A' = 1, 'B' = 2, 'C' = 3))",
		"Nullable(Enum:Foo)":        "Nullable(Enum8('' = 0, 'A' = 1, 'B' = 2, 'C' = 3))",
		"Array(Nullable(Enum:Foo))": "Array(Nullable(Enum8('' = 0, 'A' = 1, 'B' = 2, 'C' = 3)))",
		"Int64":                     "Int64",
		"Array(String)":             "Array(String)",
		"Enum:WithZeroValue":        "Enum8('' = -1, 'Zero' = 0, 'One' = 1)",
	}
	for typeStr, expectedChType := range assertions {
		chType := AsClickhouseType(tax, typeStr)
		assert.Equal(t, expectedChType, chType)
	}
}
