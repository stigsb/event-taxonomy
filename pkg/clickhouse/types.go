// Copyright 2018-2019 Zedge, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and

package clickhouse

import (
	"fmt"
	"gitlab.com/zedge-oss/zeppa/event-taxonomy/pkg/model"
	"sort"
)

// InvalidType marks a model type that could not successfully be converted to a clickhouse type
const InvalidType = "__INVALID__"

// AsClickhouseType converts the type `typeStr` in `taxonomy` to Clickhouse format
func AsClickhouseType(taxonomy *model.EventTaxonomy, typeStr string) string {
	pt, err := model.ParseType(typeStr)
	if err != nil {
		return InvalidType
	}
	scalarType := pt
	chType := ""
	chTypeSuffix := ""
	if model.IsArrayType(scalarType) {
		chType += "Array("
		chTypeSuffix = ")" + chTypeSuffix
		scalarType = scalarType.(*model.ArrayType).InnerType
	}
	if model.IsNullableType(scalarType) {
		chType += "Nullable("
		chTypeSuffix = ")" + chTypeSuffix
		scalarType = scalarType.(*model.NullableType).InnerType
	}
	switch scalarType.(type) {
	case *model.EnumType:
		enum := taxonomy.GetEnum(scalarType.(*model.EnumType).EnumName)
		if enum == nil {
			return InvalidType
		}
		if enum.MaxValue() < 128 {
			chType += "Enum8"
		} else {
			chType += "Enum16"
		}
		chType += "("
		if enum.GetFieldByValue(0) == nil {
			chType += "'' = 0" // 0 is the default enum value, mapping to empty string for tidy SELECT output
		} else {
			chType += "'' = -1"
		}
		sort.Sort(model.EnumValueSorter(enum.Fields))
		for _, field := range enum.Fields {
			chType += fmt.Sprintf(", '%s' = %d", field.Name, field.Value)
		}
		chType += ")"
	case *model.BoolType:
		chType += "UInt8"
	default:
		chType += pt.ScalarType()
	}
	return chType + chTypeSuffix
}
