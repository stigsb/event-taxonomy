// Copyright 2018-2019 Zedge, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and

package clickhouse

import (
	"testing"

	"github.com/ory/dockertest"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

const (
	testDatabase = "android"
	testTable    = "events_local"
)

var fixtureQueries = []string{
	"CREATE DATABASE " + testDatabase,
	"CREATE TABLE " + testDatabase + "." + testTable + " (" +
		"  server_date Date," +
		"  num Int32 DEFAULT -1," +
		"  mod5 Int8 MATERIALIZED (num % 5)" +
		") ENGINE = MergeTree()" +
		"  PARTITION BY toYYYYMM(server_date)" +
		"  ORDER BY (server_date, num)" +
		"  SAMPLE BY num",
}

func TestGetCurrentStructure(t *testing.T) {
	runWithClickhouseContainer(t, func(docker *dockertest.Pool, container *dockertest.Resource, db *StatefulDB) {
		for _, query := range fixtureQueries {
			_, err := db.Exec(query)
			require.NoError(t, err)
		}

		inspector := NewTableInspector(db.DB)
		table := &Table{Name: testTable, DatabaseName: testDatabase}
		require.NoError(t, inspector.InspectInto(table))
		assert.Len(t, table.Columns, 3)
		assert.Equal(t, Column{Name: "server_date", Type: "Date"}, *table.Columns[0])
		assert.Equal(t, Column{Name: "num", Type: "Int32", DefaultKind: "DEFAULT", DefaultExpression: "CAST(-1, 'Int32')"}, *table.Columns[1])
		assert.Equal(t, Column{Name: "mod5", Type: "Int8", DefaultKind: "MATERIALIZED", DefaultExpression: "num % 5"}, *table.Columns[2])

		zidCol := &Column{Name: "zid", Type: "String"}
		zidFixedCol := &Column{Name: "zid", Type: "FixedString(40)"}
		assert.NoError(t, table.AddColumn(zidCol, db))
		assert.Len(t, table.Columns, 4)
		assert.True(t, table.HasColumn("zid"))
		assert.Error(t, table.AddColumn(zidCol, db))
		zid := table.GetColumn("zid")
		assert.True(t, Column{Name: "zid", Type: "String"}.Equals(*zid))
		assert.NoError(t, table.ModifyColumn(zidFixedCol, db))
		zid = table.GetColumn("zid")
		assert.True(t, Column{Name: "zid", Type: "FixedString(40)"}.Equals(*zid))
		assert.NoError(t, table.DropColumn(zidCol, db))
		assert.Len(t, table.Columns, 3)
		assert.False(t, table.HasColumn("zid"))

		exists, err := inspector.TableExists(testDatabase, testTable)
		assert.NoError(t, err)
		assert.True(t, exists)
		exists, err = inspector.TableExists("default", "some_other_table")
		assert.NoError(t, err)
		assert.False(t, exists)

		// Stop the clickhouse container to test error conditions for TableExists and InspectInto
		_ = docker.Purge(container)
		_, err = inspector.TableExists("system", "tables")
		assert.Error(t, err)
		assert.Error(t, inspector.InspectInto(table))
	})
}
