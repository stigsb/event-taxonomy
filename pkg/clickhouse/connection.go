// Copyright 2018-2019 Zedge, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and

package clickhouse

import (
	"context"
	"database/sql"
	"fmt"
)

type StatefulDB struct {
	*sql.DB
	expressionCache map[string]string
	LogQueries      bool
	QueryLog        []string
}

func (sd *StatefulDB) Exec(query string, args ...interface{}) (sql.Result, error) {
	if sd.LogQueries {
		sd.QueryLog = append(sd.QueryLog, query)
	}
	return sd.DB.Exec(query, args...)
}

func (sd *StatefulDB) ExecContext(ctx context.Context, query string, args ...interface{}) (sql.Result, error) {
	if sd.LogQueries {
		sd.QueryLog = append(sd.QueryLog, query)
	}
	return sd.DB.ExecContext(ctx, query, args...)
}

func (sd *StatefulDB) Query(query string, args ...interface{}) (*sql.Rows, error) {
	if sd.LogQueries {
		sd.QueryLog = append(sd.QueryLog, query)
	}
	return sd.DB.Query(query, args...)
}

func (sd *StatefulDB) QueryContext(ctx context.Context, query string, args ...interface{}) (*sql.Rows, error) {
	if sd.LogQueries {
		sd.QueryLog = append(sd.QueryLog, query)
	}
	return sd.DB.QueryContext(ctx, query, args...)
}

func (sd *StatefulDB) MaybeObserveColumnNormalization(t *Table, col *Column) error {
	if col.hasExpression() {
		inspectedCol, err := t.InspectColumn(col.Name, sd)
		if err != nil {
			return err
		}
		sd.ObserveColumnNormalization(col, inspectedCol)
	}
	return nil
}

func (sd *StatefulDB) ObserveColumnNormalization(oldCol, newCol *Column) {
	if oldCol.DefaultExpression != "" && newCol.DefaultExpression != "" && oldCol.DefaultExpression != newCol.DefaultExpression {
		sd.LearnExpression(oldCol.DefaultExpression, newCol.DefaultExpression)
	}
	if oldCol.TTL != "" && newCol.TTL != "" && oldCol.TTL != newCol.TTL {
		sd.LearnExpression(oldCol.TTL, newCol.TTL)
	}
}

func (sd *StatefulDB) LearnExpression(in, normalized string) {
	sd.expressionCache[in] = normalized
}

func (sd *StatefulDB) KnowsExpression(expr string) bool {
	_, ret := sd.expressionCache[expr]
	return ret
}

func (sd *StatefulDB) NormalizeExpression(expr string) string {
	if tmp, found := sd.expressionCache[expr]; found {
		return tmp
	}
	return expr
}

func (sd *StatefulDB) OpenDB(dsn string) (*sql.DB, error) {
	connect, err := sql.Open("clickhouse", dsn)
	if err != nil {
		return nil, fmt.Errorf(`sql.Open("clickhouse", "%s"): %v`, dsn, err)
	}
	if err := connect.Ping(); err != nil {
		//if exception, ok := err.(*driver.Exception); ok {
		//	fmt.Printf("[%d] %s \n%s\n", exception.Code, exception.Message, exception.StackTrace)
		//} else {
		//	fmt.Printf("Some other error: %v\n", err)
		//}
		return nil, err
	}
	sd.DB = connect
	return sd.DB, nil
}

func NewStatefulDB() *StatefulDB {
	return &StatefulDB{expressionCache: make(map[string]string)}
}

func NewLoggingStatefulDB() *StatefulDB {
	db := NewStatefulDB()
	db.LogQueries = true
	return db
}
