// Copyright 2018-2019 Zedge, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and

package model

import (
	"encoding/json"
	"fmt"
	"strings"
)

// Lifecycle describes whether a field/property is stable or deprecated
type Lifecycle string

const (
	// PreProductionLifecycle - unsafe changes are allowed, field/property may be removed
	PreProductionLifecycle = Lifecycle("PreProduction")
	// ProductionLifecycle - changes are not allowed
	ProductionLifecycle = Lifecycle("Production")
	// ProductionEndOfLifeLifecycle - changes are not allowed, field is scheduled for removal
	ProductionEndOfLifeLifecycle = Lifecycle("ProductionEndOfLife")
	// VoidLifecycle - field is waiting to be permanently deleted and forgotten
	VoidLifecycle = Lifecycle("Void")
	// VoidLifecycle - field is waiting to be permanently deleted and forgotten
	EventNameFakeLifecycle = Lifecycle("EventName")
)

var allowedLifecycleTransitions = map[Lifecycle][]Lifecycle{
	ProductionLifecycle:          []Lifecycle{ProductionEndOfLifeLifecycle},
	ProductionEndOfLifeLifecycle: []Lifecycle{ProductionLifecycle},
	PreProductionLifecycle:       []Lifecycle{ProductionLifecycle, VoidLifecycle},
}

var stringToLifecycle = map[string]Lifecycle{
	"beta":                   PreProductionLifecycle,
	"incubating":             PreProductionLifecycle,
	"production":             ProductionLifecycle,
	"betaendoflife":          PreProductionLifecycle,
	"beta_end_of_life":       PreProductionLifecycle,
	"preproduction":          PreProductionLifecycle,
	"productionendoflife":    ProductionEndOfLifeLifecycle,
	"production_end_of_life": ProductionEndOfLifeLifecycle,
	"void":                   VoidLifecycle,
}

// CanChangeTo tells whether this lifecycle state can be changed to another
func (l *Lifecycle) CanChangeTo(to Lifecycle) bool {
	for _, ok := range allowedLifecycleTransitions[*l] {
		if to == ok {
			return true
		}
	}
	return false
}

/**
 * Revive the property
 */
func (l *Lifecycle) Revive() {
	var x Lifecycle
	switch *l {
	case VoidLifecycle:
		x = PreProductionLifecycle
	case ProductionEndOfLifeLifecycle:
		x = ProductionLifecycle
	}
	if x != "" {
		*l = x
	}
}

func (l *Lifecycle) EndLife() {
	var x Lifecycle
	switch *l {
	case PreProductionLifecycle:
		x = VoidLifecycle
	case ProductionLifecycle:
		x = ProductionEndOfLifeLifecycle
	}
	if x != "" {
		*l = x
	}
}

// UnmarshalJSON unmashals a JSON string to a Lifecycle value
func (l *Lifecycle) UnmarshalJSON(b []byte) error {
	var j string
	err := json.Unmarshal(b, &j)
	if err != nil {
		return err
	}
	id, ok := stringToLifecycle[strings.ToLower(j)]
	if !ok {
		return fmt.Errorf("invalid lifecycle: \"%s\"", j)
	}
	*l = id
	return nil
}

func (l Lifecycle) IsExisting() bool {
	return l != VoidLifecycle
}

func (l Lifecycle) IsAlive() bool {
	return l == PreProductionLifecycle || l == ProductionLifecycle
}
