// Copyright 2018-2019 Zedge, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and

package model

import (
	"encoding/json"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("For lifecycles,", func() {
	When("unmarshalling a lifecycle", func() {
		var lifecycle Lifecycle
		It("should fail on bad JSON", func() {
			Expect(json.Unmarshal([]byte(`"Invalid"`), &lifecycle)).To(MatchError(`invalid lifecycle: "Invalid"`))
			Expect(json.Unmarshal([]byte(`{}`), &lifecycle)).ToNot(Succeed())
		})
		It("should succeed on good JSON", func() {
			Expect(json.Unmarshal([]byte(`"Production"`), &lifecycle)).To(Succeed())
			Expect(lifecycle).To(Equal(ProductionLifecycle))
		})
	})
	When("marshalling a lifecycle", func() {
		It("should produce expected output", func() {
			for lifecycle, expected := range map[Lifecycle][]byte{
				PreProductionLifecycle:       []byte(`"PreProduction"`),
				ProductionLifecycle:          []byte(`"Production"`),
				ProductionEndOfLifeLifecycle: []byte(`"ProductionEndOfLife"`),
			} {
				bytes, err := json.Marshal(lifecycle)
				Expect(err).ToNot(HaveOccurred())
				Expect(bytes).To(Equal(expected))
			}
		})
	})
	When("changing between lifecycles", func() {
		LifecycleChangeShouldBeAllowed(ProductionLifecycle, ProductionEndOfLifeLifecycle)
		LifecycleChangeShouldBeAllowed(ProductionEndOfLifeLifecycle, ProductionLifecycle)
		LifecycleChangeShouldBeAllowed(PreProductionLifecycle, ProductionLifecycle)
		LifecycleChangeShouldBeAllowed(PreProductionLifecycle, VoidLifecycle)
		LifecycleChangeShouldNotBeAllowed(VoidLifecycle, ProductionLifecycle)
		LifecycleChangeShouldNotBeAllowed(VoidLifecycle, PreProductionLifecycle)
		LifecycleChangeShouldNotBeAllowed(VoidLifecycle, ProductionEndOfLifeLifecycle)
	})
	When("reviving life cycle of type", func() {
		var lifecycle Lifecycle
		JustBeforeEach(func() {
			lifecycle.Revive()
		})
		When("void", func() {
			BeforeEach(func() {
				lifecycle = "Void"
			})
			It("should be revived to PreProduction", func() {
				Expect(lifecycle).To(Equal(PreProductionLifecycle))
			})
		})
		When("preproduction", func() {
			BeforeEach(func() {
				lifecycle = "PreProduction"
			})
			It("should stay as PreProduction", func() {
				Expect(lifecycle).To(Equal(PreProductionLifecycle))
			})
		})
		When("production", func() {
			BeforeEach(func() {
				lifecycle = "Production"
			})
			It("should stay as Production", func() {
				Expect(lifecycle).To(Equal(ProductionLifecycle))
			})
		})
		When("production end of life", func() {
			BeforeEach(func() {
				lifecycle = "ProductionEndOfLife"
			})
			It("should be revived to Production", func() {
				Expect(lifecycle).To(Equal(ProductionLifecycle))
			})
		})
	})
})

func LifecycleChangeShouldBeAllowed(from, to Lifecycle) {
	Specify(string(from)+" -> "+string(to)+" should be allowed", func() {
		Expect(from.CanChangeTo(to)).To(BeTrue())
	})
}

func LifecycleChangeShouldNotBeAllowed(from, to Lifecycle) {
	Specify(string(from)+" -> "+string(to)+" should not be allowed", func() {
		Expect(from.CanChangeTo(to)).To(BeFalse())
	})
}
