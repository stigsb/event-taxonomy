// Copyright 2018-2019 Zedge, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and

package model

import (
	"errors"
	"fmt"
	"time"

	"github.com/google/go-cmp/cmp"
)

const minMonthsBeforeAllowedToRemoveField = 18

var minTimeBeforeAllowedToRemoveField time.Duration

func init() {
	minTimeBeforeAllowedToRemoveField = time.Hour * 24 * 30 * minMonthsBeforeAllowedToRemoveField
}

// Validate returns a list of errors if there are invalid/unsafe changes
func (c *TaxonomyUpdate) Validate() (issues []error) {
	if c.AppID != nil {
		issues = append(issues, errors.New("taxonomy \"appid\" is immutable"))
	}
	for _, change := range c.UpdatedProperties {
		for _, issue := range change.DetectIssues(c.BlacklistedProperties) {
			issues = append(issues, fmt.Errorf("property \"%s\": %v", change.From.Name, issue))
		}
	}
	for _, removed := range c.RemovedProperties {
		if removed.Lifecycle == ProductionLifecycle {
			issues = append(issues, fmt.Errorf(
				"property \"%s\": can not remove when lifecycle is \"Production\" (change lifecycle to \"ProductionEndOfLife\" instead)", removed.Name))
		}
		// TODO(stigsb): check that enough time has elapsed since the property went into EOL state before allowing the change
	}
	for _, change := range c.UpdatedEnums {
		for _, issue := range change.DetectIssues(c.BlacklistedProperties) {
			issues = append(issues, fmt.Errorf("enum \"%s\": %v", change.Name, issue))
		}
	}
	return
}

// DetectIssues returns an non-empty slice of errors if the enum change is not allowed (empty if OK).
func (e *EnumUpdate) DetectIssues(blacklistedProperties []string) (issues []error) {
	for _, change := range e.UpdatedFields {
		for _, issue := range change.DetectIssues(blacklistedProperties) {
			issues = append(issues, issue)
		}
	}
	for _, removed := range e.RemovedFields {
		if removed.Lifecycle == ProductionLifecycle {
			issues = append(issues, fmt.Errorf(
				"field \"%s\": can not remove enum field with \"Production\" lifecycle (change to \"ProductionEndOfLife\" lifecycle instead)", removed.Name))
		}
		// TODO(stigsb): check that enough time has elapsed since the field went into EOL state before allowing the change
	}
	return
}

// DetectIssues returns an error if the enum field change is not allowed, or `nil` if it is.
func (f *EnumFieldUpdate) DetectIssues(blacklistedProperties []string) (errors []error) {
	from := f.From
	to := f.To
	if stringInSlice(from.Name, blacklistedProperties) {
		errors = append(errors, fmt.Errorf("property \"%s\" is blacklisted!", from.Name))
	}
	if stringInSlice(to.Name, blacklistedProperties) {
		errors = append(errors, fmt.Errorf("property \"%s\" is blacklisted!", to.Name))
	}
	if from.Value != to.Value {
		errors = append(errors, fmt.Errorf("field \"%s\": must not change value", f.From.Name))
	}
	if from.Lifecycle != to.Lifecycle && !from.Lifecycle.CanChangeTo(to.Lifecycle) {
		errors = append(errors, fmt.Errorf("field \"%s\": invalid lifecycle change: %s -> %s", from.Name, string(from.Lifecycle), string(to.Lifecycle)))
	}
	return
}

// DetectIssues returns an array of errors if these are changes in the property that are not
// allowed (or an empty array if there are no issues).
//
// The rules are:
//  - Type: type changes must be "safe", except for "beta" properties
//  - Scope: "production" properties can not change scope away from "envelope"
//  - Lifecycle: any change ok, except for "envelope" or "internal" properties
//
func (c *PropertyUpdate) DetectIssues(blacklistedProperties []string) (errors []error) {
	from := c.From
	to := c.To

	if stringInSlice(from.Name, blacklistedProperties) {
		errors = append(errors, fmt.Errorf("property \"%s\" is blacklisted", from.Name))
	}
	if stringInSlice(to.Name, blacklistedProperties) {
		errors = append(errors, fmt.Errorf("property \"%s\" is blacklisted", to.Name))
	}

	if cmp.Equal(from, to) {
		return
	}

	if from.Name != to.Name {
		errors = append(errors, fmt.Errorf("not allowed to change property name"))
	}
	if from.Scope == InternalScope || from.Scope == EnvelopeScope {
		errors = append(errors, fmt.Errorf("not allowed to change envelope or internal properties"))
		return
	}

	if from.Type != to.Type {
		if from.Lifecycle != PreProductionLifecycle && !IsSafeTypeChange(from.Type, to.Type) {
			errors = append(errors, fmt.Errorf("invalid type change: %s -> %s", from.Type, to.Type))
		}
	}

	if from.Lifecycle != to.Lifecycle && !from.Lifecycle.CanChangeTo(to.Lifecycle) {
		errors = append(errors, fmt.Errorf("invalid lifecycle change: %s -> %s", string(from.Lifecycle), string(to.Lifecycle)))
	}

	return
}

func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}
