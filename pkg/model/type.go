// Copyright 2018-2019 Zedge, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and

package model

import (
	"fmt"
	"math"
	"regexp"
	"strconv"
)

// Constants for type names
const (
	Int8TypeName     = "Int8"
	Int16TypeName    = "Int16"
	Int32TypeName    = "Int32"
	Int64TypeName    = "Int64"
	Float32TypeName  = "Float32"
	Float64TypeName  = "Float64"
	EnumTypeName     = "Enum"
	DateTypeName     = "Date"
	DateTimeTypeName = "DateTime"
	StringTypeName   = "String"
	BoolTypeName     = "Bool"
)

var (
	safeScalarTypeChanges = map[string][]string{
		BoolTypeName:  []string{Int8TypeName, Int16TypeName, Int32TypeName, Int64TypeName},
		Int8TypeName:  []string{Int16TypeName, Int32TypeName, Int64TypeName, StringTypeName, Float32TypeName, Float64TypeName, BoolTypeName},
		Int16TypeName: []string{Int32TypeName, Int64TypeName, StringTypeName, Float32TypeName, Float64TypeName},
		Int32TypeName: []string{Int64TypeName, StringTypeName, Float32TypeName, Float64TypeName},
		Int64TypeName: []string{StringTypeName, Float32TypeName, Float64TypeName},
		EnumTypeName:  []string{Int16TypeName, Int32TypeName, Int64TypeName, StringTypeName, Float32TypeName, Float64TypeName},
	}
	typeRegexp = regexp.MustCompile("^(?:" +
		"Bool|Int(?:8|16|32|64)|Date|DateTime|String|Enum:[A-Za-z_][A-Za-z0-9_]+|Float(?:32|64)|" +
		"Array\\((?:Bool|Int(?:8|16|32|64)|String|Date|DateTime|Enum:[A-Za-z_][A-Za-z0-9_]+|Float(?:32|64))\\)" +
		")$")
	intTypeRegexp   = regexp.MustCompile(`^Int(8|16|32|64)$`)
	floatTypeRegexp = regexp.MustCompile(`^Float(8|16|32|64)$`)
	//fixedStringTypeRegexp = regexp.MustCompile(`^FixedString\((\d+)\)$`)
	//lowCardinalityTypeRegexp = regexp.MustCompile(`^LowCardinality\((.+)\)$`)
	scalarRegexp       = regexp.MustCompile(`^(Bool|Int(?:8|16|32|64)|String|Date|DateTime|Enum:[A-Za-z_][A-Za-z0-9_]+|Float(?:32|64))$`)
	arrayTypeRegexp    = regexp.MustCompile(`^Array\((.*)\)$`)
	nullableTypeRegexp = regexp.MustCompile(`^Nullable\((.+)\)$`)
	enumTypeRegexp     = regexp.MustCompile(`^Enum:([A-Za-z_][A-Za-z0-9_]*)$`)
	dateRegexp         = regexp.MustCompile(`^\d{4}-\d{2}-\d{2}$`)
	dateTimeRegexp     = regexp.MustCompile(`^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$`)
)

// IsSafeTypeChange returns true if `fromType` can be safely altered to `toType`
func IsSafeTypeChange(fromStr string, toStr string) bool {
	fromType, err := ParseType(fromStr)
	if err != nil {
		return false
	}
	toType, err := ParseType(toStr)
	if err != nil {
		return false
	}
	if IsArrayType(fromType) != IsArrayType(toType) {
		// Can not convert from array to scalar or vice-versa
		return false
	}
	if types, ok := safeScalarTypeChanges[fromType.ScalarType()]; ok == true {
		for _, safeType := range types {
			if safeType == toType.ScalarType() {
				return true
			}
		}
	}
	return false
}

func ParseType(typeStr string) (Type, error) {
	if m := arrayTypeRegexp.FindStringSubmatch(typeStr); m != nil {
		inner, err := ParseScalarType(m[1])
		if err != nil {
			return nil, err // TODO: wrap error
		}
		return &ArrayType{InnerType: inner}, nil
	}
	return ParseScalarType(typeStr)
}

func IsNullableType(t Type) bool {
	_, nullable := t.(*NullableType)
	return nullable
}

func IsArrayType(t Type) bool {
	_, isArray := t.(*ArrayType)
	return isArray
}

func IsEnumType(t Type) bool {
	return t.ScalarType() == "Enum"
}

func ParseScalarType(typeStr string) (Type, error) {
	if m := nullableTypeRegexp.FindStringSubmatch(typeStr); m != nil {
		inner, err := ParseScalarType(m[1])
		if err != nil {
			return nil, err // TODO: wrap error
		}
		if IsNullableType(inner) {
			return nil, fmt.Errorf(`can not nest Nullable()`)
		}
		return &NullableType{InnerType: inner}, nil
	}
	if m := intTypeRegexp.FindStringSubmatch(typeStr); m != nil {
		bits, err := strconv.Atoi(m[1])
		if err != nil {
			return nil, fmt.Errorf(`invalid number of Int bits: %w`, err)
		}
		return &IntType{Bits: bits}, nil
	}
	if m := floatTypeRegexp.FindStringSubmatch(typeStr); m != nil {
		bits, err := strconv.Atoi(m[1])
		if err != nil {
			return nil, fmt.Errorf(`invalid number of Float bits: %w`, err)
		}
		return &FloatType{Bits: bits}, nil
	}
	if m := enumTypeRegexp.FindStringSubmatch(typeStr); m != nil {
		return &EnumType{EnumName: m[1]}, nil
	}
	switch typeStr {
	case DateTypeName:
		return &DateType{}, nil
	case DateTimeTypeName:
		return &DateTimeType{}, nil
	case BoolTypeName:
		return &BoolType{}, nil
	case StringTypeName:
		return &StringType{}, nil
	}
	return nil, fmt.Errorf(`invalid type: %q`, typeStr)
}

func isValidType(typeStr string) error {
	if _, err := ParseType(typeStr); err != nil {
		return err
	}
	return nil
}

//+kubebuilder:object:generate=false
type Type interface {
	ScalarType() string
	String() string
}

//+kubebuilder:object:generate=false
type IntType struct {
	Bits int
}

func (t IntType) String() string {
	return t.ScalarType()
}

var _ Type = &IntType{}

func (t IntType) ScalarType() string {
	return "Int" + strconv.Itoa(t.Bits)
}

func (t IntType) MaxValue() int {
	switch t.Bits {
	case 8:
		return math.MaxInt8
	case 16:
		return math.MaxInt16
	case 32:
		return math.MaxInt32
	}
	return math.MaxInt64
}

func (t IntType) MinValue() int {
	switch t.Bits {
	case 8:
		return math.MinInt8
	case 16:
		return math.MinInt16
	case 32:
		return math.MinInt32
	}
	return math.MinInt64
}

//+kubebuilder:object:generate=false
type FloatType struct {
	Bits int
}

func (t FloatType) String() string {
	return t.ScalarType()
}

var _ Type = &FloatType{}

func (t FloatType) ScalarType() string {
	return "Float" + strconv.Itoa(t.Bits)
}

func (t FloatType) MaxValue() float64 {
	if t.Bits == 32 {
		return math.MaxFloat32
	}
	return math.MaxFloat64
}

func (t FloatType) MinValue() float64 {
	if t.Bits == 32 {
		return -math.MaxFloat32
	}
	return -math.MaxFloat64
}

//+kubebuilder:object:generate=false
type NullableType struct {
	InnerType Type
}

var _ Type = &NullableType{}

func (t NullableType) String() string {
	return "Nullable(" + t.InnerType.String() + ")"
}

func (t NullableType) ScalarType() string {
	return t.InnerType.ScalarType()
}

func (t NullableType) IsArray() bool {
	// Arrays can not be nullable, only empty
	return false
}

func (t NullableType) IsNullable() bool {
	return true
}

//+kubebuilder:object:generate=false
type ArrayType struct {
	InnerType Type
}

func (t ArrayType) String() string {
	return "Array(" + t.InnerType.String() + ")"
}

func (t ArrayType) ScalarType() string {
	return t.InnerType.ScalarType()
}

var _ Type = &ArrayType{}

//+kubebuilder:object:generate=false
type EnumType struct {
	EnumName string
}

func (t EnumType) ScalarType() string {
	return "Enum"
}

func (t EnumType) String() string {
	return "Enum:" + t.EnumName
}

var _ Type = &EnumType{}

//+kubebuilder:object:generate=false
type StringType struct{}

func (t StringType) String() string {
	return t.ScalarType()
}

func (t StringType) ScalarType() string {
	return "String"
}

var _ Type = &StringType{}

//+kubebuilder:object:generate=false
type FixedStringType struct {
	Length int
}

func (t FixedStringType) String() string {
	return "FixedString(" + strconv.Itoa(t.Length) + ")"
}

func (t FixedStringType) ScalarType() string {
	return "String"
}

var _ Type = &FixedStringType{}

//+kubebuilder:object:generate=false
type DateType struct{}

func (t DateType) String() string {
	return t.ScalarType()
}

func (t DateType) ScalarType() string {
	return "Date"
}

var _ Type = &DateType{}

//+kubebuilder:object:generate=false
type DateTimeType struct{}

func (t DateTimeType) String() string {
	return t.ScalarType()
}

func (t DateTimeType) ScalarType() string {
	return "DateTime"
}

var _ Type = &DateTimeType{}

//+kubebuilder:object:generate=false
type BoolType struct{}

func (t BoolType) String() string {
	return t.ScalarType()
}

func (t BoolType) ScalarType() string {
	return "Bool"
}

var _ Type = &BoolType{}
