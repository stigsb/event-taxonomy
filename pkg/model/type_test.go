// Copyright 2018-2019 Zedge, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and

package model

import (
	"fmt"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("For types,", func() {
	When("parsing type strings", func() {
		ItShouldParse("Enum:Foo").As(&EnumType{EnumName: "Foo"})
		ItShouldParse("Int8").As(&IntType{Bits: 8})
		ItShouldParse("Array(Enum:Foo)").As(&ArrayType{InnerType: &EnumType{EnumName: "Foo"}})
		ItShouldParse("Nullable(Enum:Foo)").As(&NullableType{InnerType: &EnumType{EnumName: "Foo"}})
		ItShouldFailParsing("Nullable(Array(String))")
		ItShouldFailParsing("Int128")
	})
	When("validating types", func() {
		validScalarTypes := []string{
			"Int8",
			"Int16",
			"Int32",
			"Int64",
			"Date",
			"DateTime",
			"Enum:Foo",
			"Float32",
			"Float64",
			"String",
		}
		for _, stringType := range validScalarTypes {
			arrayType := "Array(" + stringType + ")"
			nullableType := "Nullable(" + stringType + ")"
			arrayNullableType := "Array(Nullable(" + stringType + "))"
			nullableArrayType := "Nullable(Array(" + stringType + "))"
			It(fmt.Sprintf("should accept %q", stringType), func() {
				Expect(isValidType(stringType)).To(Succeed())
			})
			It(fmt.Sprintf("should accept %q", arrayType), func() {
				Expect(isValidType(arrayType)).To(Succeed())
			})
			It(fmt.Sprintf("should accept %q", nullableType), func() {
				Expect(isValidType(nullableType)).To(Succeed())
			})
			It(fmt.Sprintf("should accept %q", arrayNullableType), func() {
				Expect(isValidType(arrayNullableType)).To(Succeed())
			})
			It(fmt.Sprintf("should not accept %q", nullableArrayType), func() {
				Expect(isValidType(nullableArrayType)).ToNot(Succeed())
			})
		}
	})
	When("testing for type change safety", func() {
		ItShouldBeSafe("Bool", "Int8")
		ItShouldBeSafe("Bool", "Int8")
		ItShouldBeSafe("Bool", "Int16")
		ItShouldBeSafe("Bool", "Int32")
		ItShouldBeSafe("Bool", "Int64")
		ItShouldBeSafe("Int8", "Bool")
		ItShouldBeSafe("Int8", "Int16")
		ItShouldBeSafe("Int8", "Int32")
		ItShouldBeSafe("Int8", "Int32")
		ItShouldBeSafe("Int8", "Int64")
		ItShouldBeSafe("Int8", "String")
		ItShouldNotBeSafe("Int16", "Bool")
		ItShouldNotBeSafe("Int16", "Int8")
		ItShouldNotBeSafe("Int32", "Bool")
		ItShouldNotBeSafe("Int32", "Int16")
		ItShouldNotBeSafe("Int64", "Bool")
		ItShouldNotBeSafe("String", "Int16")
		ItShouldBeSafe("Enum:Foo", "Int16")
		ItShouldNotBeSafe("Enum:Foo", "Int8")
		ItShouldBeSafe("Enum:Foo", "Int32")
		ItShouldBeSafe("Enum:Foo", "Int64")
		ItShouldNotBeSafe("Enum:Foo", "Array(Int16)")
	})
})

type itAs struct {
	Type string
}

func (a *itAs) As(exp Type) {
	p, err := ParseType(a.Type)
	It(fmt.Sprintf("should parse %q", a.Type), func() {
		Expect(err).ToNot(HaveOccurred())
		Expect(p).To(Equal(exp))
	})
}

func ItShouldParse(typ string) *itAs {
	ia := &itAs{Type: typ}
	return ia
}

func ItShouldFailParsing(typ string) {
	_, err := ParseType(typ)
	It(fmt.Sprintf("should fail parsing %q", typ), func() {
		Expect(err).ToNot(Succeed())
	})
}

func nullable(t string) string {
	return "Nullable(" + t + ")"
}

func array(t string) string {
	return "Array(" + t + ")"
}

func ItShouldBeSafe(from, to string) {
	It(fmt.Sprintf("should be safe to change from %q to %q", from, to), func() {
		Expect(IsSafeTypeChange(from, to)).To(BeTrue())
	})
	It(fmt.Sprintf("should be safe to change from %q to %q", nullable(from), nullable(to)), func() {
		Expect(IsSafeTypeChange(nullable(from), nullable(to))).To(BeTrue())
	})
	It(fmt.Sprintf("should be safe to change from %q to %q", array(from), array(to)), func() {
		Expect(IsSafeTypeChange(array(from), array(to))).To(BeTrue())
	})
	It(fmt.Sprintf("should be safe to change from %q to %q", array(nullable(from)), array(nullable(to))), func() {
		Expect(IsSafeTypeChange(array(nullable(from)), array(nullable(to)))).To(BeTrue())
	})
}

func ItShouldNotBeSafe(from, to string) {
	It(fmt.Sprintf("should not be safe to change from %q to %q", from, to), func() {
		Expect(IsSafeTypeChange(from, to)).To(BeFalse())
	})
}
