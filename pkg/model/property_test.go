// Copyright 2018-2019 Zedge, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and

package model

import (
	"encoding/json"
	"fmt"
	"math"
	"time"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var (
	propertyTestTaxonomyV1 = &EventTaxonomy{Properties: []*EventProperty{{Name: "a"}, {Name: "b"}}}
	propertyTestTaxonomyV2 = &EventTaxonomy{Properties: []*EventProperty{{Name: "a"}, {Name: "b"}, {Name: "c"}}}
	lastMod                = metav1.Time{Time: time.Now()}
)

var _ = Describe("For properties,", func() {
	When("validating properties", func() {
		var (
			goodProp1 = EventProperty{"foo", EventScope, ProductionLifecycle, lastMod, "Int32"}
			goodProp2 = EventProperty{"foo", InternalScope, ProductionLifecycle, lastMod, "Int16"}
		)
		It("should accept good properties", func() {
			Expect(goodProp1.OK()).To(Succeed())
			Expect(goodProp2.OK()).To(Succeed())
		})
		It("should reject properties with invalid names", func() {
			prop := EventProperty{":foo", EventScope, ProductionLifecycle, lastMod, "Int32"}
			Expect(prop.OK()).To(MatchError(MatchRegexp(`^name must match regexp .*`)))
		})
		It("should reject properties with invalid types", func() {
			prop := EventProperty{":foo", EventScope, ProductionLifecycle, lastMod, "Int36"}
			Expect(prop.OK()).To(MatchError(MatchRegexp(`^name must match regexp .*`)))
		})
	})
	Context("computing differences:", func() {
		When("adding a property", func() {
			It("should be returned by PropertiesAddedIn", func() {
				Expect(propertyTestTaxonomyV1.PropertiesAddedIn(propertyTestTaxonomyV2)).To(Equal([]string{"c"}))
			})
		})
		When("removing a property", func() {
			It("should be returned by PropertiesRemovedIn", func() {
				Expect(propertyTestTaxonomyV2.PropertiesRemovedIn(propertyTestTaxonomyV1)).To(Equal([]string{"c"}))
			})
		})
		When("preserving properties", func() {
			It("they should all be returned by PropertiesPreservedIn", func() {
				Expect(propertyTestTaxonomyV2.PropertiesPreservedIn(propertyTestTaxonomyV1)).To(Equal([]string{"a", "b"}))
			})
		})
	})

	When("dealing with types and values", func() {
		It("should handle integer json.Number as int", func() {
			Expect(intValue(42)).To(Equal(42))
			Expect(intValue(json.Number("42"))).To(Equal(42))
		})
		It("should handle float json.Number as float32", func() {
			Expect(isFloat32(math.MaxFloat32)).To(BeTrue())
			Expect(isFloat32(json.Number(fmt.Sprintf("%f", math.MaxFloat32)))).To(BeTrue())
		})
		It("should recognize unix timestamps", func() {
			Expect(isUnixTime(float64(math.MaxUint32))).To(BeTrue())
			Expect(isUnixTime(float64(math.MaxUint32 + 1))).To(BeFalse())
			Expect(isUnixTime(float64(0))).To(BeTrue())
		})
	})

	When("testing for assignability", func() {
		taxonomy := &EventTaxonomy{
			Enums: []*EventEnum{
				{
					Name: "FOO",
					Fields: []*EventEnumField{
						{Name: "A", Value: 1},
						{Name: "B", Value: 2},
						{Name: "C", Value: 3},
					},
				},
			},
			Properties: []*EventProperty{
				{Name: "i8", Type: "Int8"},
				{Name: "i8Arr", Type: "Array(Int8)"},
				{Name: "i16", Type: "Int16"},
				{Name: "i16Arr", Type: "Array(Int16)"},
				{Name: "i32", Type: "Int32"},
				{Name: "i32Arr", Type: "Array(Int32)"},
				{Name: "i64", Type: "Int64"},
				{Name: "i64Arr", Type: "Array(Int64)"},
				{Name: "f32", Type: "Float32"},
				{Name: "f32Arr", Type: "Array(Float32)"},
				{Name: "f64", Type: "Float64"},
				{Name: "f64Arr", Type: "Array(Float64)"},
				{Name: "enum", Type: "Enum:FOO"},
				{Name: "enumArr", Type: "Array(Enum:FOO)"},
				{Name: "str", Type: "String"},
				{Name: "strArr", Type: "Array(String)"},
				{Name: "date", Type: "Date"},
				{Name: "dateArr", Type: "Array(Date)"},
				{Name: "datetime", Type: "DateTime"},
				{Name: "datetimeArr", Type: "Array(DateTime)"},
				{Name: "bool", Type: "Bool"},
				{Name: "boolArr", Type: "Array(Bool)"},
			},
		}
		taxonomy.BuildIndex()

		ExpectValueIsAssignableToProperty := func(property, value string) {
			Specify(fmt.Sprintf("%q should be assignable to property %q", value, property), func() {
				Expect(taxonomy.GetProperty(property).IsAssignableFrom(mustDecode(value), taxonomy)).To(BeTrue())
			})
		}

		ExpectValueIsNotAssignableToProperty := func(property, value string) {
			Specify(fmt.Sprintf("%q should not be assignable to property %q", value, property), func() {
				ExpectWithOffset(2, taxonomy.GetProperty(property).IsAssignableFrom(mustDecode(value), taxonomy)).To(BeFalse())
			})
		}

		ExpectValueIsAssignableToProperty("i8", fmt.Sprintf("%d", math.MinInt8))
		ExpectValueIsAssignableToProperty("i8", fmt.Sprintf("%d", math.MaxInt8))
		ExpectValueIsNotAssignableToProperty("i8", fmt.Sprintf("%d", math.MinInt8-1))
		ExpectValueIsNotAssignableToProperty("i8", fmt.Sprintf("%d", math.MaxInt8+1))
		ExpectValueIsNotAssignableToProperty("i8", `[0]`)
		ExpectValueIsAssignableToProperty("i8Arr", fmt.Sprintf("[%d,%d]", math.MinInt8, math.MaxInt8))
		ExpectValueIsNotAssignableToProperty("i8Arr", fmt.Sprintf("[%d,%d]", math.MinInt8-1, math.MaxInt8+1))
		ExpectValueIsNotAssignableToProperty("i8Arr", `0`)

		ExpectValueIsAssignableToProperty("i16", fmt.Sprintf("%d", math.MinInt16))
		ExpectValueIsAssignableToProperty("i16", fmt.Sprintf("%d", math.MaxInt16))
		ExpectValueIsNotAssignableToProperty("i16", fmt.Sprintf("%d", math.MinInt16-1))
		ExpectValueIsNotAssignableToProperty("i16", fmt.Sprintf("%d", math.MaxInt16+1))
		ExpectValueIsNotAssignableToProperty("i16", `[0]`)
		ExpectValueIsAssignableToProperty("i16Arr", fmt.Sprintf("[%d,%d]", math.MinInt16, math.MaxInt16))
		ExpectValueIsNotAssignableToProperty("i16Arr", fmt.Sprintf("[%d,%d]", math.MinInt16-1, math.MaxInt16+1))
		ExpectValueIsNotAssignableToProperty("i16Arr", `0`)

		ExpectValueIsAssignableToProperty("i32", fmt.Sprintf("%d", math.MinInt32))
		ExpectValueIsAssignableToProperty("i32", fmt.Sprintf("%d", math.MaxInt32))
		ExpectValueIsNotAssignableToProperty("i32", fmt.Sprintf("%d", math.MinInt32-1))
		ExpectValueIsNotAssignableToProperty("i32", fmt.Sprintf("%d", math.MaxInt32+1))
		ExpectValueIsNotAssignableToProperty("i32", `[0]`)
		ExpectValueIsAssignableToProperty("i32Arr", fmt.Sprintf("[%d,%d]", math.MinInt32, math.MaxInt32))
		ExpectValueIsNotAssignableToProperty("i32Arr", fmt.Sprintf("[%d,%d]", math.MinInt32-1, math.MaxInt32+1))
		ExpectValueIsNotAssignableToProperty("i32Arr", `0`)

		ExpectValueIsAssignableToProperty("i64", fmt.Sprintf("%d", math.MinInt64))
		ExpectValueIsAssignableToProperty("i64", fmt.Sprintf("%d", math.MaxInt64))
		ExpectValueIsNotAssignableToProperty("i64", `[0]`)
		ExpectValueIsAssignableToProperty("i64Arr", fmt.Sprintf("[%d,%d]", math.MinInt64, math.MaxInt64))
		ExpectValueIsNotAssignableToProperty("i64Arr", `0`)

		ExpectValueIsAssignableToProperty("f32", fmt.Sprintf("%f", math.MaxFloat32))
		ExpectValueIsAssignableToProperty("f32", fmt.Sprintf("%f", -math.MaxFloat32))
		ExpectValueIsNotAssignableToProperty("f32", fmt.Sprintf("%f", math.MaxFloat64))
		ExpectValueIsNotAssignableToProperty("f32", fmt.Sprintf("%f", -math.MaxFloat64))
		ExpectValueIsNotAssignableToProperty("f32", `[0]`)
		ExpectValueIsAssignableToProperty("f32Arr", fmt.Sprintf("[%f,%f]", math.MaxFloat32, -math.MaxFloat32))
		ExpectValueIsNotAssignableToProperty("f32Arr", fmt.Sprintf("[%f,%f]", math.MaxFloat64, -math.MaxFloat64))
		ExpectValueIsNotAssignableToProperty("f32Arr", `0`)

		ExpectValueIsAssignableToProperty("f64", fmt.Sprintf("%f", math.MaxFloat64))
		ExpectValueIsAssignableToProperty("f64", fmt.Sprintf("%f", -math.MaxFloat64))
		ExpectValueIsNotAssignableToProperty("f64", `[0]`)
		ExpectValueIsAssignableToProperty("f64Arr", fmt.Sprintf("[%f,%f]", math.MaxFloat64, -math.MaxFloat64))
		ExpectValueIsNotAssignableToProperty("f64Arr", `0`)

		ExpectValueIsAssignableToProperty("enum", `"A"`)
		ExpectValueIsNotAssignableToProperty("enum", `"D"`)
		ExpectValueIsNotAssignableToProperty("enum", `1`)
		ExpectValueIsNotAssignableToProperty("enum", `["A"]`)

		ExpectValueIsAssignableToProperty("str", `"string"`)
		ExpectValueIsNotAssignableToProperty("str", `0`)
		ExpectValueIsNotAssignableToProperty("str", `["string"]`)
		ExpectValueIsAssignableToProperty("strArr", `["string"]`)
		ExpectValueIsNotAssignableToProperty("strArr", `[0]`)
		ExpectValueIsNotAssignableToProperty("strArr", `"string"`)

		ExpectValueIsAssignableToProperty("date", `"2018-01-01"`)
		ExpectValueIsNotAssignableToProperty("date", `20180101`)
		ExpectValueIsAssignableToProperty("dateArr", `["2018-01-01"]`)
		ExpectValueIsNotAssignableToProperty("dateArr", `[20180101]`)
		ExpectValueIsNotAssignableToProperty("dateArr", `"2018-01-01"`)

		ExpectValueIsAssignableToProperty("datetime", fmt.Sprintf("%d", math.MaxUint32))
		ExpectValueIsAssignableToProperty("datetime", `0`)
		ExpectValueIsNotAssignableToProperty("datetime", `-1`)
		ExpectValueIsNotAssignableToProperty("datetime", fmt.Sprintf("%d", math.MaxUint32+1))
		ExpectValueIsAssignableToProperty("datetime", `"2018-01-01 00:00:00"`)
		ExpectValueIsNotAssignableToProperty("datetime", `"197X-XX-00 00:00:00"`)
		ExpectValueIsNotAssignableToProperty("datetime", `[0]`)
		ExpectValueIsNotAssignableToProperty("datetime", `["2018-01-01 00:00:00"]`)
		ExpectValueIsAssignableToProperty("datetimeArr", `[0]`)
		ExpectValueIsAssignableToProperty("datetimeArr", fmt.Sprintf("[%d]", math.MaxUint32))
		ExpectValueIsAssignableToProperty("datetimeArr", fmt.Sprintf("[0,%d]", math.MaxUint32))
		ExpectValueIsNotAssignableToProperty("datetimeArr", `[-1]`)
		ExpectValueIsNotAssignableToProperty("datetimeArr", fmt.Sprintf("[%d]", math.MaxUint32+1))
		ExpectValueIsAssignableToProperty("datetimeArr", `["2018-01-01 00:00:00"]`)
		ExpectValueIsNotAssignableToProperty("datetimeArr", `["197X-XX-00 00:00:00"]`)
		ExpectValueIsAssignableToProperty("datetimeArr", `[0]`)
		ExpectValueIsNotAssignableToProperty("datetimeArr", `"2018-01-01 00:00:00"`)

		ExpectValueIsAssignableToProperty("bool", `true`)
		ExpectValueIsAssignableToProperty("bool", `false`)
		ExpectValueIsAssignableToProperty("boolArr", `[true,false]`)
		ExpectValueIsAssignableToProperty("bool", `0`)
		ExpectValueIsAssignableToProperty("bool", `1`)
		ExpectValueIsAssignableToProperty("bool", `255`)
		ExpectValueIsNotAssignableToProperty("bool", `256`)
		ExpectValueIsNotAssignableToProperty("bool", `-1`)
		ExpectValueIsNotAssignableToProperty("bool", `"true"`)
		ExpectValueIsNotAssignableToProperty("boolArr", `[-1, 256]`)
		ExpectValueIsNotAssignableToProperty("boolArr", `[true, -1]`)

	})
})

func mustDecode(s string) interface{} {
	var v interface{}
	if err := json.Unmarshal([]byte(s), &v); err != nil {
		panic(err)
	}
	return v
}
