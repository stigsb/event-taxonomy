// Copyright 2018-2019 Zedge, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and

package model

import (
	"fmt"
	"math"
	"sort"
	"time"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EventEnumField represents a single field within an enum
type EventEnumField struct {
	Name         string      `json:"name"`
	Value        int         `json:"value"`
	Lifecycle    Lifecycle   `json:"lifecycle"`
	LastModified metav1.Time `json:"last_modified"`
}

type EnumValueSorter []*EventEnumField

func (a EnumValueSorter) Len() int           { return len(a) }
func (a EnumValueSorter) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a EnumValueSorter) Less(i, j int) bool { return a[i].Value < a[j].Value }

// FieldNames returns a list of strings with names of all our enum fields
func (e *EventEnum) FieldNames() []string {
	var names = make([]string, len(e.Fields))
	for i, field := range e.Fields {
		names[i] = field.Name
	}
	sort.Strings(names)
	return names
}

// OK validates this object and returns an error if there is an issue
func (f *EventEnumField) OK() error {
	if err := isValidName(f.Name); err != nil {
		return err
	}
	if f.Value < math.MinInt16 || f.Value > math.MaxInt16 {
		return fmt.Errorf("value (%d) must be in the range [%d, %d]", f.Value, math.MinInt16, math.MaxInt16)
	}
	return nil
}

func (f *EventEnumField) SameAs(other *EventEnumField) bool {
	// Disregard LastModified
	return f.Name == other.Name &&
		f.Value == other.Value &&
		f.Lifecycle == other.Lifecycle
}

func (f *EventEnumField) Revive() {
	if !f.Lifecycle.IsAlive() {
		f.Lifecycle.Revive()
		f.LastModified = v1.Time{Time: time.Now()}
	}
}

func (f *EventEnumField) EndLife() {
	if f.Lifecycle.IsAlive() {
		f.Lifecycle.EndLife()
		f.LastModified = v1.Time{Time: time.Now()}
	}
}

// FieldsAddedIn returns a list of names of enum fields added in `new`
func (e *EventEnum) FieldsAddedIn(new *EventEnum) []string {
	return difference(new.FieldNames(), e.FieldNames())
}

// FieldsRemovedIn returns a list of names of enum fields removed in `new`
func (e *EventEnum) FieldsRemovedIn(new *EventEnum) []string {
	return difference(e.FieldNames(), new.FieldNames())
}

// FieldsPreservedIn returns a list of names of enum fields that are in both this object and `new`
func (e *EventEnum) FieldsPreservedIn(new *EventEnum) []string {
	return intersection(e.FieldNames(), new.FieldNames())
}
