// Copyright 2018-2019 Zedge, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and

package model

import (
	"fmt"
)

// EventEnum :
type EventEnum struct {
	Name     string                     `json:"name"`
	Fields   []*EventEnumField          `json:"fields"`
	FieldMap map[string]*EventEnumField `json:"-"`
	ValueMap map[int]*EventEnumField    `json:"-"`
}

func (e *EventEnum) BuildIndex() {
	e.FieldMap = make(map[string]*EventEnumField)
	e.ValueMap = make(map[int]*EventEnumField)
	for _, field := range e.Fields {
		e.FieldMap[field.Name] = field
		e.ValueMap[field.Value] = field
	}
}

// GetField :
func (e *EventEnum) GetField(name string) *EventEnumField {
	return e.FieldMap[name]
}

// GetFieldByValue returns the enum field with a given value, or nil if not found
func (e *EventEnum) GetFieldByValue(value int) *EventEnumField {
	return e.ValueMap[value]
}

// OK :
func (e *EventEnum) OK() error {
	if err := isValidName(e.Name); err != nil {
		return err
	}
	var seenFieldNames = make(map[string]bool)
	var seenFieldValues = make(map[int]string)
	for _, field := range e.Fields {
		if err := field.OK(); err != nil {
			return fmt.Errorf("field \"%s\": %v", field.Name, err)
		}
		if _, seen := seenFieldNames[field.Name]; seen == true {
			return fmt.Errorf("duplicate field name: \"%s\"", field.Name)
		}
		seenFieldNames[field.Name] = true
		if f, seen := seenFieldValues[field.Value]; seen == true {
			return fmt.Errorf("re-used value (%d) in fields \"%s\" and \"%s\"", field.Value, f, field.Name)
		}
		seenFieldValues[field.Value] = field.Name
	}
	return nil
}

// EnumsAddedIn returns a list of names of enums added in `newModel`
func (t *EventTaxonomy) EnumsAddedIn(newModel *EventTaxonomy) []string {
	return difference(newModel.EnumNames(), t.EnumNames())
}

// EnumsRemovedIn returns a list of removed event property names
func (t *EventTaxonomy) EnumsRemovedIn(newModel *EventTaxonomy) []string {
	return difference(t.EnumNames(), newModel.EnumNames())
}

// EnumsPreservedIn returns a list names of enums in both taxonomies
func (t *EventTaxonomy) EnumsPreservedIn(newModel *EventTaxonomy) []string {
	return intersection(t.EnumNames(), newModel.EnumNames())
}

// MaxValue returns the highest value in one of our enum fields
func (e *EventEnum) MaxValue() int {
	max := 0
	for _, f := range e.Fields {
		if f.Lifecycle.IsExisting() && f.Value > max {
			max = f.Value
		}
	}
	return max
}

func (e *EventEnum) NextValue() int {
	next := 1
start:
	for _, f := range e.Fields {
		if f.Value == next {
			next++
			goto start
		}
	}
	return next
}

func (e *EventEnum) SameAs(other *EventEnum) bool {
	if e.Name != other.Name || len(e.Fields) != len(other.Fields) {
		return false
	}
	for i := range e.Fields {
		if !e.Fields[i].SameAs(other.Fields[i]) {
			return false
		}
	}
	return true
}
