// Copyright 2018-2019 Zedge, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and

package model

import (
	"fmt"
)

func (t *EventTaxonomy) ValidateTwin(twin EventTaxonomy) (issues []error) {
	for _, prop := range t.Properties {
		if twinProp := twin.GetProperty(prop.Name); twinProp != nil {
			if twinProp.Type != prop.Type {
				issues = append(issues, fmt.Errorf("property %q type mismatch with twin taxonomy %q", prop.Name, twin.AppID))
			}
			if twinProp.Scope != prop.Scope {
				issues = append(issues, fmt.Errorf("property %q scope mismatch with twin taxonomy %q", prop.Name, twin.AppID))
			}
		}
	}
	sharedEnums := make([]string, 0)
	for _, enum := range t.Enums {
		if twinEnum := twin.GetEnum(enum.Name); twinEnum != nil {
			for _, field := range enum.Fields {
				if twinField := twinEnum.GetField(field.Name); twinField != nil {
					if field.Value != twinField.Value {
						issues = append(issues, fmt.Errorf("enum %q field %q name/value mismatch with twin taxonomy %q", enum.Name, field.Name, twin.AppID))
					}
				}
				if twinField := twinEnum.GetFieldByValue(field.Value); twinField != nil {
					if field.Name != twinField.Name {
						issues = append(issues, fmt.Errorf("enum %q field %q value/name mismatch with twin taxonomy %q", enum.Name, field.Name, twin.AppID))
					}
				}
			}
			sharedEnums = append(sharedEnums, enum.Name)
		}
	}
	return
}
