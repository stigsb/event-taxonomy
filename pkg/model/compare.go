// Copyright 2018-2019 Zedge, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and

package model

import (
	"fmt"
)

// PropertyUpdate :
type PropertyUpdate struct {
	From *EventProperty `json:"from"`
	To   *EventProperty `json:"to"`
}

// EnumFieldUpdate describes a changed enum field
type EnumFieldUpdate struct {
	From *EventEnumField `json:"from"`
	To   *EventEnumField `json:"to"`
}

// EnumUpdate describes the changes to an enum.
type EnumUpdate struct {
	Name          string             `json:"name"`
	AddedFields   []*EventEnumField  `json:"addedFields,omitempty"`
	RemovedFields []*EventEnumField  `json:"removedFields,omitempty"`
	UpdatedFields []*EnumFieldUpdate `json:"updatedFields,omitempty"`
}

// AppIDUpdate represents a changed "appid" for an EventTaxonomy
type AppIDUpdate struct {
	From string `json:"from"`
	To   string `json:"to"`
}

// LifecycleUpdate represents a changed "lifecycle" for an EventTaxonomy
type LifecycleUpdate struct {
	From Lifecycle `json:"from"`
	To   Lifecycle `json:"to"`
}

// TaxonomyUpdate represents the changes needed to migrate from one version
// of the event taxonomy to another.
type TaxonomyUpdate struct {
	AppID                 *AppIDUpdate      `json:"appid,omitempty"`
	AddedEnums            []*EventEnum      `json:"addedEnums,omitempty"`
	RemovedEnums          []*EventEnum      `json:"removedEnums,omitempty"`
	UpdatedEnums          []*EnumUpdate     `json:"updatedEnums,omitempty"`
	AddedProperties       []*EventProperty  `json:"addedProperties,omitempty"`
	RemovedProperties     []*EventProperty  `json:"removedProperties,omitempty"`
	UpdatedProperties     []*PropertyUpdate `json:"updatedProperties,omitempty"`
	BlacklistedProperties []string          `json:"blacklistedProperties,omitempty"`
}

func newEnumUpdate(name string) *EnumUpdate {
	var change EnumUpdate
	change.Name = name
	change.AddedFields = make([]*EventEnumField, 0)
	change.RemovedFields = make([]*EventEnumField, 0)
	change.UpdatedFields = make([]*EnumFieldUpdate, 0)
	return &change
}

func newTaxonomyUpdate() *TaxonomyUpdate {
	var summary TaxonomyUpdate
	summary.AddedEnums = make([]*EventEnum, 0)
	summary.RemovedEnums = make([]*EventEnum, 0)
	summary.UpdatedEnums = make([]*EnumUpdate, 0)
	summary.AddedProperties = make([]*EventProperty, 0)
	summary.RemovedProperties = make([]*EventProperty, 0)
	summary.UpdatedProperties = make([]*PropertyUpdate, 0)
	return &summary
}

// ComputeChanges computes the changes needed to migrate between taxonomy versions
// and returns a `TaxonomyUpdate`
func ComputeChanges(oldT, newT *EventTaxonomy) *TaxonomyUpdate {
	changes := newTaxonomyUpdate()

	// Keeping blacklist reference available for changes later.
	changes.BlacklistedProperties = oldT.BlacklistPropertiesFilterNames()

	if oldT.AppID != newT.AppID {
		changes.AppID = &AppIDUpdate{oldT.AppID, newT.AppID}
	}
	for _, addedPropName := range oldT.PropertiesAddedIn(newT) {
		changes.AddedProperties = append(changes.AddedProperties, newT.GetProperty(addedPropName))
	}
	for _, removedPropName := range oldT.PropertiesRemovedIn(newT) {
		changes.RemovedProperties = append(changes.RemovedProperties, oldT.GetProperty(removedPropName))
	}
	for _, propName := range oldT.PropertiesPreservedIn(newT) {
		oldProp := oldT.GetProperty(propName)
		newProp := newT.GetProperty(propName)
		if !oldProp.SameAs(newProp) {
			changes.UpdatedProperties = append(changes.UpdatedProperties, &PropertyUpdate{From: oldProp, To: newProp})
		}
	}
	for _, addedEnumName := range oldT.EnumsAddedIn(newT) {
		changes.AddedEnums = append(changes.AddedEnums, newT.GetEnum(addedEnumName))
	}
	for _, removedEnumName := range oldT.EnumsRemovedIn(newT) {
		changes.RemovedEnums = append(changes.RemovedEnums, oldT.GetEnum(removedEnumName))
	}
	for _, enumName := range oldT.EnumsPreservedIn(newT) {
		var oldEnum = oldT.GetEnum(enumName)
		var newEnum = newT.GetEnum(enumName)
		if oldEnum.SameAs(newEnum) {
			continue
		}
		enumUpdate := newEnumUpdate(enumName)
		for _, addedFieldName := range oldEnum.FieldsAddedIn(newEnum) {
			enumUpdate.AddedFields = append(enumUpdate.AddedFields, newEnum.GetField(addedFieldName))
		}
		for _, removedFieldName := range oldEnum.FieldsRemovedIn(newEnum) {
			enumUpdate.RemovedFields = append(enumUpdate.RemovedFields, oldEnum.GetField(removedFieldName))
		}
		for _, fieldName := range oldEnum.FieldsPreservedIn(newEnum) {
			oldField := oldEnum.GetField(fieldName)
			newField := newEnum.GetField(fieldName)
			if !oldField.SameAs(newField) {
				c := &EnumFieldUpdate{From: oldField, To: newField}
				enumUpdate.UpdatedFields = append(enumUpdate.UpdatedFields, c)
			}
		}
		changes.UpdatedEnums = append(changes.UpdatedEnums, enumUpdate)
	}

	return changes
}

// ComputeChangesFromFiles computes the changes needed to migrate between taxonomy versions
// read from `oldFile and `newFile`, and returns a `TaxonomyUpdate` object.
func ComputeChangesFromFiles(oldFile, newFile string) (*TaxonomyUpdate, error) {

	var oldT, newT *EventTaxonomy
	var err error
	if oldT, err = LoadTaxonomyFromFile(oldFile); err != nil {
		return nil, fmt.Errorf("while loading taxonomy from \"%s\": %v", oldFile, err)
	}
	if newT, err = LoadTaxonomyFromFile(newFile); err != nil {
		return nil, fmt.Errorf("while loading taxonomy from \"%s\": %v", newFile, err)
	}
	return ComputeChanges(oldT, newT), nil
}
