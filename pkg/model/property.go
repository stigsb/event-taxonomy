// Copyright 2018-2019 Zedge, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and

package model

import (
	"encoding/json"
	"fmt"
	"math"
	"regexp"
	"time"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EventProperty represents a single property (user, event or envelope) in the event taxonomy
type EventProperty struct {
	Name         string        `json:"name"`
	Scope        PropertyScope `json:"scope"`
	Lifecycle    Lifecycle     `json:"lifecycle"`
	LastModified metav1.Time   `json:"last_modified"`
	Type         string        `json:"type"`
}

func intValue(v interface{}) int {
	if i, ok := v.(int); ok {
		return i
	}
	if n, ok := v.(json.Number); ok {
		i, err := n.Int64()
		if err != nil {
			panic(fmt.Sprintf("could not get int64 value from json.Number %s: %v", v, err))
		}
		return int(i)
	}
	panic(fmt.Sprintf("could not get intValue from %v", v))
}

func isWithinIntRange(i int, min, max int) bool {
	return i >= min && i <= max
}

func isFloat32(v interface{}) bool {
	if _, ok := v.(float32); ok {
		return true
	}
	if f, ok := v.(float64); ok {
		return f >= math.SmallestNonzeroFloat32 && f <= math.MaxFloat32
	}
	if n, ok := v.(json.Number); ok {
		if f, err := n.Float64(); err == nil {
			return f >= math.SmallestNonzeroFloat32 && f <= math.MaxFloat32
		}
	}
	return false
}

func isFloat64(v interface{}) bool {
	if _, ok := v.(float64); ok {
		return true
	}
	if n, ok := v.(json.Number); ok {
		if _, err := n.Float64(); err == nil {
			return true
		}
	}
	return false
}

func isString(v interface{}) bool {
	_, ok := v.(string)
	return ok
}

func isUnixTime(f float64) bool {
	i := int(f)
	if i < 0 {
		return false
	}
	if i > math.MaxUint32 {
		return false
	}
	return true
	// return i >= 0 && i <= math.MaxUint32
}

func isIsoDate(s string) bool {
	return dateRegexp.MatchString(s)
}

func isIsoTime(s string) bool {
	return dateTimeRegexp.MatchString(s)
}

func allStrings(v []string, f func(string) bool) bool {
	for _, s := range v {
		if !f(s) {
			return false
		}
	}
	return true
}

func allInts(v []int, f func(int) bool) bool {
	for _, i := range v {
		if !f(i) {
			return false
		}
	}
	return true
}

func allFloat32(v []float32, f func(float32) bool) bool {
	for _, e := range v {
		if !f(e) {
			return false
		}
	}
	return true
}

func allFloat64(v []float64, f func(float64) bool) bool {
	for _, e := range v {
		if !f(e) {
			return false
		}
	}
	return true
}

func matchAll(strings []string, r *regexp.Regexp) bool {
	for _, s := range strings {
		if !r.MatchString(s) {
			return false
		}
	}
	return true
}

func isAssignableFrom(t Type, v interface{}, taxonomy *EventTaxonomy) bool {
	// Ref. https://golang.org/pkg/encoding/json/#Unmarshal for the types used below.
	switch v.(type) {
	case bool:
		_, ok := t.(*BoolType)
		return ok
	case float64:
		if IsArrayType(t) {
			return false
		}
		f := v.(float64)
		floatType, isFloat := t.(*FloatType)
		if isFloat {
			return f >= floatType.MinValue() && f <= floatType.MaxValue()
		}
		intType, isInt := t.(*IntType)
		if isInt {
			i := int(f)
			return i >= intType.MinValue() && i <= intType.MaxValue()
		}
		if _, isBool := t.(*BoolType); isBool {
			i := int(f)
			return i >= 0 && i <= 255
		}
		if _, isDateTime := t.(*DateTimeType); isDateTime {
			return isUnixTime(f)
		}
		return false
	case string:
		switch t.(type) {
		case *StringType:
			return true
		case *DateType:
			return isIsoDate(v.(string))
		case *DateTimeType:
			return isIsoTime(v.(string))
		case *EnumType:
			return taxonomy.GetEnum(t.(*EnumType).EnumName).GetField(v.(string)) != nil
		}
		return false
	case []interface{}:
		if !IsArrayType(t) {
			return false
		}
		innerType := t.(*ArrayType).InnerType
		for _, e := range v.([]interface{}) {
			if !isAssignableFrom(innerType, e, taxonomy) {
				return false
			}
		}
		return true
	case nil:
		return IsNullableType(t)
	case map[string]interface{}:
		// We do not support JSON objects, only arrays
		return false
	default:
		return false
	}
}

// IsAssignableFrom tells whether this property's value may be assigned from `v`.
// The input is assumed to come from json.Unmarshal()-ing into a map[string]interface{},
// such that numeric values can be float64 or json.Number.
func (p *EventProperty) IsAssignableFrom(v interface{}, taxonomy *EventTaxonomy) bool {
	t, err := ParseType(p.Type)
	if err != nil {
		return false
	}
	return isAssignableFrom(t, v, taxonomy)
}

// GetLifecycles returns Production, PreProduction or nil
func (p *EventProperty) GetLifecycles(v interface{}, taxonomy *EventTaxonomy) []Lifecycle {
	t, err := ParseType(p.Type)
	if err != nil || !isAssignableFrom(t, v, taxonomy) {
		return []Lifecycle{}
	}
	if !IsEnumType(t) {
		return []Lifecycle{p.Lifecycle}
	}
	enum := taxonomy.EnumMap[t.(*EnumType).EnumName]
	if IsArrayType(t) {
		values := v.([]interface{})
		if len(values) == 0 {
			return []Lifecycle{p.Lifecycle}
		}
		var lifecycles []Lifecycle
		for _, l := range values {
			lifecycles = append(lifecycles, enum.FieldMap[l.(string)].Lifecycle)
		}
		return lifecycles
	}
	return []Lifecycle{enum.FieldMap[v.(string)].Lifecycle}
}

// OK validates this object and returns an error if there is an issue
func (p *EventProperty) OK() error {
	if err := isValidName(p.Name); err != nil {
		return err
	}
	if err := isValidType(p.Type); err != nil {
		return err
	}
	return nil
}

// PropertiesAddedIn returns a list of properties added in `newModel`
func (t *EventTaxonomy) PropertiesAddedIn(newModel *EventTaxonomy) []string {
	return difference(newModel.PropertyNames(), t.PropertyNames())
}

// PropertiesRemovedIn returns a list of properties removed in `newModel`
func (t *EventTaxonomy) PropertiesRemovedIn(newModel *EventTaxonomy) []string {
	return difference(t.PropertyNames(), newModel.PropertyNames())
}

// PropertiesPreservedIn returns a list of properties preserved in `newModel`
func (t *EventTaxonomy) PropertiesPreservedIn(newModel *EventTaxonomy) []string {
	return intersection(newModel.PropertyNames(), t.PropertyNames())
}

func (p *EventProperty) SameAs(other *EventProperty) bool {
	// Disregard LastModified
	return p.Name == other.Name &&
		p.Type == other.Type &&
		p.Lifecycle == other.Lifecycle &&
		p.Scope == other.Scope
}

func (f *EventProperty) Revive() {
	if !f.Lifecycle.IsAlive() {
		f.Lifecycle.Revive()
		f.LastModified = v1.Time{Time: time.Now()}
	}
}

func (f *EventProperty) EndLife() {
	if f.Lifecycle.IsAlive() {
		f.Lifecycle.EndLife()
		f.LastModified = v1.Time{Time: time.Now()}
	}
}
