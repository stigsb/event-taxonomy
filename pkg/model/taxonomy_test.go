// Copyright 2018-2019 Zedge, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and

package model

import (
	"path/filepath"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("For taxonomies,", func() {
	var (
		tax *EventTaxonomy
		err error
	)
	When("loading taxonomy from file", func() {
		BeforeEach(func() {
			tax, err = LoadTaxonomyFromFile(filepath.Join("..", "..", "testdata", "android.json"))
		})
		It(`should load successfully and have appid "android"`, func() {
			Expect(err).ToNot(HaveOccurred())
			Expect(tax.AppID).To(Equal("android"))
		})
	})

	When("getting properties via index", func() {
		BeforeEach(func() {
			tax = &EventTaxonomy{
				Enums: []*EventEnum{
					{Name: "A", Fields: []*EventEnumField{}},
				},
				Properties: []*EventProperty{
					{Name: "foo", Lifecycle: PreProductionLifecycle, Scope: UserScope, Type: "Int32"},
					{Name: "bar", Lifecycle: PreProductionLifecycle, Scope: UserScope, Type: "Int32"},
				},
			}
		})
		It("will not return stuff before calling .BuildIndex()", func() {
			Expect(tax.GetEnum("A")).To(BeNil())
			Expect(tax.GetProperty("foo")).To(BeNil())
			Expect(tax.GetProperty("bar")).To(BeNil())
		})
		It("should return stuff after calling .BuildIndex()", func() {
			tax.BuildIndex()
			Expect(tax.GetEnum("A").Name).To(Equal("A"))
			Expect(tax.GetProperty("foo").Name).To(Equal("foo"))
			Expect(tax.GetProperty("bar").Name).To(Equal("bar"))
		})
	})

	When("calling BlacklistPropertiesFilterNames()", func() {
		unsorted := []string{"z", "a", "o"}
		sorted := []string{"a", "o", "z"}
		It("should return a sorted list of property names", func() {
			Expect((&EventTaxonomy{BlacklistPropertiesFilter: unsorted}).BlacklistPropertiesFilterNames()).To(Equal(sorted))
		})
	})

	When("validating a taxonomy", func() {
		It("should reject invalid enum names", func() {
			tax = &EventTaxonomy{Enums: []*EventEnum{{Name: ":invalid"}}}
			Expect(tax.OK()).To(MatchError(MatchRegexp(`enum ":invalid": name must match regexp .*`)))
		})
		It("should reject duplicate enum names", func() {
			tax = &EventTaxonomy{Enums: []*EventEnum{{Name: "valid"}, {Name: "valid"}}}
			Expect(tax.OK()).To(MatchError(`duplicate enum name: "valid"`))
		})
		It("should reject invalid property names", func() {
			tax = &EventTaxonomy{Properties: []*EventProperty{{Name: ":invalid"}}}
			Expect(tax.OK()).To(MatchError(MatchRegexp(`property ":invalid": name must match regexp .*`)))
		})
		It("should reject duplicate property names", func() {
			validProp := &EventProperty{Name: "valid", Scope: EventScope, Lifecycle: PreProductionLifecycle, Type: "Int32"}
			tax = &EventTaxonomy{Properties: []*EventProperty{validProp, validProp}}
			Expect(tax.OK()).To(MatchError(`duplicate property name: "valid"`))
		})
	})

	When("validating names", func() {
		It("should reject names shorter than two characters", func() {
			Expect(isValidName("")).ToNot(Succeed())
			Expect(isValidName("A")).ToNot(Succeed())
		})
		It("should reject names containing invalid characters", func() {
			Expect(isValidName(".A")).ToNot(Succeed())
		})
		It("should accept valid names", func() {
			Expect(isValidName("AA")).To(Succeed())
			Expect(isValidName("Abcdefghij0123456789Abcdefghij0123456789Abcdefghij0123456789Abc")).To(Succeed())
		})
		It("should reject names longer than 63 characters", func() {
			Expect(isValidName("Abcdefghij0123456789Abcdefghij0123456789Abcdefghij0123456789Abcd")).ToNot(Succeed())
		})
	})
})
