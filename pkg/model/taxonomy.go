// Copyright 2018-2019 Zedge, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and

package model

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"regexp"
	"sort"
)

var (
	nameRegexp = regexp.MustCompile("^[A-Za-z_][A-Za-z0-9_]{1,62}$")
)

// EventTaxonomy contains the event taxonomy for a single product.
type EventTaxonomy struct {
	AppID                     string                    `json:"appid"`
	Enums                     []*EventEnum              `json:"enums,omitempty"`
	Properties                []*EventProperty          `json:"properties"`
	BlacklistPropertiesFilter []string                  `json:"blacklist_properties_filter,omitempty"`
	EnumMap                   map[string]*EventEnum     `json:"-"`
	PropertyMap               map[string]*EventProperty `json:"-"`
}

// LoadTaxonomyFromFile creates an EventTaxonomy object from a JSON file
func LoadTaxonomyFromFile(fileName string) (*EventTaxonomy, error) {
	var taxonomy EventTaxonomy
	data, err := ioutil.ReadFile(fileName)
	if err != nil {
		return nil, err
	}
	if err = json.Unmarshal(data, &taxonomy); err != nil {
		return nil, err
	}
	if err = taxonomy.OK(); err != nil {
		return nil, err
	}
	taxonomy.buildIndex()
	return &taxonomy, nil
}

// BuildIndex creates lookup maps for enums, enum fields and properties
func (t *EventTaxonomy) BuildIndex() {
	t.buildIndex()
}

func (t *EventTaxonomy) buildIndex() {
	t.EnumMap = make(map[string]*EventEnum)
	t.PropertyMap = make(map[string]*EventProperty)
	for _, enum := range t.Enums {
		t.EnumMap[enum.Name] = enum
		enum.BuildIndex()
	}
	for _, prop := range t.Properties {
		t.PropertyMap[prop.Name] = prop
	}
}

// EnumNames returns a sorted array of just the enum names from the taxonomy
func (t *EventTaxonomy) EnumNames() []string {
	var names = make([]string, len(t.Enums))
	for i, enum := range t.Enums {
		names[i] = enum.Name
	}
	sort.Strings(names)
	return names
}

// PropertyNames returns a sorted array of just the property names from the taxonomy
func (t *EventTaxonomy) PropertyNames() []string {
	var names = make([]string, len(t.Properties))
	for i, property := range t.Properties {
		names[i] = property.Name
	}
	sort.Strings(names)
	return names
}

func (t *EventTaxonomy) RemoveEnum(name string) {
	idx := -1
	for i, enum := range t.Enums {
		if enum.Name == name {
			idx = i
			break
		}
	}
	if idx != -1 {
		t.Enums = append(t.Enums[:idx], t.Enums[idx+1:]...)
	}
}

// Blacklist properties names , sorted.
func (t *EventTaxonomy) BlacklistPropertiesFilterNames() []string {
	var properties = make([]string, len(t.BlacklistPropertiesFilter))
	copy(properties, t.BlacklistPropertiesFilter)
	sort.Strings(properties)
	return properties
}

// GetEnum :
func (t *EventTaxonomy) GetEnum(name string) *EventEnum {
	return t.EnumMap[name]
}

// GetProperty :
func (t *EventTaxonomy) GetProperty(name string) *EventProperty {
	return t.PropertyMap[name]
}

// OK validates the object and returns nil if things are a-ok, or an error if not
func (t *EventTaxonomy) OK() error {
	var seenEnumNames = make(map[string]bool)
	for _, enum := range t.Enums {
		if err := enum.OK(); err != nil {
			return fmt.Errorf("enum \"%s\": %v", enum.Name, err)
		}
		if _, seen := seenEnumNames[enum.Name]; seen == true {
			return fmt.Errorf("duplicate enum name: \"%s\"", enum.Name)
		}
		seenEnumNames[enum.Name] = true
	}
	var seenPropNames = make(map[string]bool)
	for _, property := range t.Properties {
		if err := property.OK(); err != nil {
			return fmt.Errorf("property \"%s\": %v", property.Name, err)
		}
		if _, seen := seenPropNames[property.Name]; seen == true {
			return fmt.Errorf("duplicate property name: \"%s\"", property.Name)
		}
		seenPropNames[property.Name] = true
	}
	return nil
}

func isValidName(name string) error {
	if !nameRegexp.MatchString(name) {
		return fmt.Errorf("name must match regexp %q", nameRegexp)
	}
	return nil
}
