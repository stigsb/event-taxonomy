// Copyright 2018-2019 Zedge, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and

package model

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

const (
	appID     = "my-taxonomy"
	twinAppID = "my-twin"
)

var _ = Describe("For twin taxonomies,", func() {
	var twin, update EventTaxonomy
	BeforeEach(func() {
		twin = EventTaxonomy{
			AppID: twinAppID,
			Properties: []*EventProperty{
				{Name: "user", Type: "Int64", Scope: UserScope},
				{Name: "id", Type: "Int32", Scope: EventScope},
				{Name: "name", Type: "String", Scope: EventScope},
				{Name: "type", Type: "Enum:Type", Scope: EventScope},
			},
			Enums: []*EventEnum{
				{
					Name: "Type",
					Fields: []*EventEnumField{
						{Name: "FIRST", Value: 1},
						{Name: "SECOND", Value: 2},
					},
				},
			},
		}
		twin.DeepCopyInto(&update)
		update.AppID = appID
	})
	JustBeforeEach(func() {
		twin.BuildIndex()
		update.BuildIndex()
	})

	ExpectNoIssues := func(issues []error) {
		Expect(issues).To(HaveLen(0))
	}
	When("adding a new property that the twin does not have", func() {
		It("should succeed", func() {
			update.Properties = append(update.Properties, &EventProperty{Name: "another", Type: "Int8", Scope: EventScope})
			ExpectNoIssues(update.ValidateTwin(twin))
		})
	})

	When("adding a new enum field that the twin does not have", func() {
		It("should succeed", func() {
			update.Enums[0].Fields = append(update.Enums[0].Fields, &EventEnumField{Name: "THIRD", Value: 3})
			ExpectNoIssues(update.ValidateTwin(twin))
		})
	})

	When("removing an enum field", func() {
		It("should succeed", func() { // but the TaxonomyUpdate validation will catch it
			update.Enums[0].Fields = update.Enums[0].Fields[1:]
			ExpectNoIssues(update.ValidateTwin(twin))
		})
	})

	When("removing a property", func() {
		It("should succeed", func() { // but the TaxonomyUpdate validation will catch it
			update.Properties = update.Properties[1:]
			ExpectNoIssues(update.ValidateTwin(twin))
		})
	})

	When("making a type change in conflict with the twin", func() {
		BeforeEach(func() {
			twin.DeepCopyInto(&update)
			update.Properties[0].Type = "Int32"
		})
		It("should reject the change", func() {
			issues := update.ValidateTwin(twin)
			Expect(issues).To(HaveLen(1))
			Expect(issues[0]).To(MatchError(`property "user" type mismatch with twin taxonomy "my-twin"`))
		})
	})
	When("making a scope change in conflict with the twin", func() {
		BeforeEach(func() {
			twin.DeepCopyInto(&update)
			update.AppID = appID
			update.Properties[1].Scope = UserScope
		})
		It("should reject the change", func() {
			issues := update.ValidateTwin(twin)
			Expect(issues).To(HaveLen(1))
			Expect(issues[0]).To(MatchError(`property "id" scope mismatch with twin taxonomy "my-twin"`))
		})
	})

	When("reusing enum field names and values in conflict with the twin taxonomy", func() {
		BeforeEach(func() {
			twin.DeepCopyInto(&update)
			update.AppID = appID
			update.Enums[0].Fields[1].Name = "FIRST"
			update.Enums[0].Fields[0].Name = "ZEROTH"
		})
		It("should reject the change", func() {
			issues := update.ValidateTwin(twin)
			Expect(issues).To(HaveLen(3))
			Expect(issues[0]).To(MatchError(`enum "Type" field "ZEROTH" value/name mismatch with twin taxonomy "my-twin"`))
			Expect(issues[1]).To(MatchError(`enum "Type" field "FIRST" name/value mismatch with twin taxonomy "my-twin"`))
			Expect(issues[2]).To(MatchError(`enum "Type" field "FIRST" value/name mismatch with twin taxonomy "my-twin"`))
		})
	})
})
