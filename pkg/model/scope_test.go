// Copyright 2018-2019 Zedge, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and

package model

import (
	"encoding/json"
	"fmt"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("For scopes,", func() {
	When("unmarshalling", func() {
		It("should accept valid scopes", func() {
			goodJSON := []byte(`"Envelope"`)
			var scope PropertyScope
			Expect(json.Unmarshal(goodJSON, &scope)).To(Succeed())
		})
		It("should reject invalid scopes", func() {
			badJSON := []byte(`"Invalid"`)
			badJSON2 := []byte(`{}`)
			var scope PropertyScope
			Expect(json.Unmarshal(badJSON, &scope)).To(MatchError(`invalid scope: "Invalid"`))
			Expect(json.Unmarshal(badJSON2, &scope)).NotTo(Succeed())
		})
	})
	When("marshalling", func() {
		for scope, expected := range map[PropertyScope]string{
			EnvelopeScope: "Envelope",
			EventScope:    "Event",
			UserScope:     "User",
			InternalScope: "Internal",
		} {
			It(fmt.Sprintf("should accept scope %q", scope), func() {
				bytes, err := json.Marshal(scope)
				expectedBytes := []byte(`"`)
				expectedBytes = append(expectedBytes, []byte(expected)...)
				expectedBytes = append(expectedBytes, []byte(`"`)...)
				Expect(bytes).To(Equal(expectedBytes))
				Expect(err).ToNot(HaveOccurred())
			})
		}
	})
})
