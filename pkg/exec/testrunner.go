// Copyright 2018-2019 Zedge, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and

package exec

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
	osexec "os/exec"
	"reflect"
	"strconv"
	"testing"
)

type TestRunner struct {
	ExpectedCommand []string
	Output          []byte
	ExtraEnv        map[string]string
	ExitCode        int
	TestHelperFunc  string
}

func (r TestRunner) Run(command string, args ...string) ([]byte, error) {
	return r.RunWithContext(context.Background(), command, args...)
}

func (r TestRunner) RunWithContext(ctx context.Context, command string, args ...string) ([]byte, error) {
	helperFunc := "TestHelperProcess"
	if r.TestHelperFunc != "" {
		helperFunc = r.TestHelperFunc
	}
	cs := []string{"-test.run=" + helperFunc, "--", command}
	cs = append(cs, args...)
	cmd := osexec.CommandContext(ctx, os.Args[0], cs...)
	cmd.Env = []string{
		"GO_WANT_HELPER_PROCESS=1",
		"GO_HELPER_MOCK_STDOUT=" + string(r.Output), // may need to base64 encode here?
		fmt.Sprintf("GO_HELPER_MOCK_EXIT_CODE=%d", r.ExitCode),
	}
	if r.ExtraEnv != nil {
		for k, v := range r.ExtraEnv {
			cmd.Env = append(cmd.Env, k+"="+v)
		}
	}
	if r.ExpectedCommand != nil {
		jsonArr, err := json.Marshal(r.ExpectedCommand)
		if err != nil {
			return nil, err
		}
		cmd.Env = append(cmd.Env, "GO_HELPER_EXPECTED_COMMAND_JSON="+string(jsonArr))
	}
	out, err := cmd.CombinedOutput()
	return out, err
}

func InsideHelperProcess(t *testing.T) {
	InsideHelperProcessWithCallback(t, func(t *testing.T) {})
}

func InsideHelperProcessWithCallback(t *testing.T, callback func(t *testing.T)) {
	if os.Getenv("GO_WANT_HELPER_PROCESS") != "1" {
		return
	}
	exitCode, err := strconv.Atoi(os.Getenv("GO_HELPER_MOCK_EXIT_CODE"))
	if err != nil {
		t.Fatalf("could not parse GO_HELPER_MOCK_EXIT_CODE: %v", err)
	}
	if expCmdEnv, found := os.LookupEnv("GO_HELPER_EXPECTED_COMMAND_JSON"); found {
		var expectedCommand []string
		actualCommand := os.Args[3:]
		err = json.Unmarshal([]byte(expCmdEnv), &expectedCommand)
		if err != nil {
			t.Fatalf("could not parse GO_HELPER_EXPECTED_COMMAND_JSON: %v", err)
		}
		if len(expectedCommand) > 0 {
			if !reflect.DeepEqual(actualCommand, expectedCommand) {
				t.Fatalf("expected argv %v, got %v", expectedCommand, actualCommand)
			}
		}
	}
	fmt.Print(os.Getenv("GO_HELPER_MOCK_STDOUT"))
	callback(t)
	os.Exit(exitCode)
}
