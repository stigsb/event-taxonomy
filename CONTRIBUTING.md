# Contributing

Requirements:

 * Go 1.13+
 * [kubebuilder 2.x](https://book.kubebuilder.io/quick-start.html#installation)

## Installation and preparation

Before building, add kubebuilder/bin to your PATH as described in
[the documentation](https://book.kubebuilder.io/quick-start.html#installation).

Since this project uses go modules, you do not have to worry about GOPATH, and the
code doesn't have to be checked out in $GOPATH/src.

## Project Structure

This project contains three main components:

 1. The `EventTaxonomy` API definition, located in [pkg/apis/dwh/v1beta1/event_taxonomy_types.go](pkg/apis/dwh/v1beta1/event_taxonomy_types.go)
 2. The controller code, at [pkg/controllers/eventtaxonomy_controller.go](pkg/controllers/eventtaxonomy_controller.go)
 3. The actual controller manager binary, located at [cmd/manager/main.go](cmd/manager/main.go)

## Making Changes

Tests can be run with:

    make test

The controller tests will spin up etcd/kube-apiserver from your kubebuilder installation by default. NOTE! This requires
that you have installed Kubebuilder 2.x in `/usr/local/kubebuilder` (otherwise tests will fail or time out).

If you want to run the tests in an existing cluster, set the environment variable `USE_EXISTING_CLUSTER=true` while
running tests. This will use your current kube context. Be advised that if doing this, you need to ensure that the
`eventtaxonomies.dwh.zedge.io` CRD does not exist before running tests, and they are not cleaned up after. This is a useful
command line to run tests on an existing cluster:

    kubectl delete crd eventtaxonomies.dwh.zedge.io; USE_EXISTING_CLUSTER=true make test

### Merge Request Checklist

 * If making changes to the API, the generated code (zz_generated.deepcopy.go) and config (config/**) should be
   committed along with your changes.
 * If you add, remove or update dependencies, run `go mod tidy` and commit the changes it makes.
 * Ensure your code has gone through gofmt (`make test` or `make fmt` will enforce this).

### API Compatibility Considerations

Changes to the CRD schema need to be backwards-compatible. This means you should not introduce new required elements
unless you have a robust way to generate them from old objects.

Kubernetes CRDs have a system for
[versioning your custom resources](https://kubernetes.io/docs/tasks/access-kubernetes-api/custom-resources/custom-resource-definition-versioning/)
as of release 1.16, but this release it not available in GKE at the time of writing. Once this is available, we should
default to using this mechanism for making non-trivial API changes. Non-trivial means structural changes, adding a
single optional field is a trivial change, and does not really require bumping the API version.

One important premise for Kubernetes CRD versioning is being able to do lossless conversion between versions (the
kubebuilder framework helps you set up conversion webhook code). This can be a tough requirement to meet, but is
necessary to handle all the cases that come up when mixing API versions implemented in controllers vs defined in CRDs
vs used in the actual custom resources. If you decide not to follow this premise, you will again have to carefully plan
how to deploy updates.

## Updating Copyrights

The Copyright header in generated files should be generated from [LICENSE.txt](LICENSE.txt).

For other files, the project includes an IntelliJ Copyright Profile located in [.idea/copyright/](.idea/copyright/)
