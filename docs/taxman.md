# Taxonomy Manager API (taxman)

The `taxman` API has the following endpoints:

## GET /taxonomy/{appid}

Fetches the current taxonomy for `{appid}` as JSON.

## PUT /taxonomy/{appid}

Updates the `{appid}` taxonomy after doing various [validations](validation.md) of the update.
On success it will return HTTP 200 and `{"msg": "something"}`. If validation fails, it will return
HTTP 400 and `{"issues":["issue1","issue2"],"changes":[...]}`. 

## PUT /taxonomy/{appid}?dry-run=true

A dry-run of updating the taxonomy, applies validations and returns HTTP 200 if the update would succeed,
or HTTP 400 and a list of issues like above if it would fail.

## POST /taxonomy?validate=true

Does a plain validation of the taxonomy in the POST body. No change validation is done here,
only schema and semantic validation of the posted taxonomy. If you want

## GET /topics/{appid}

Returns a list of Kafka topics associated with `{appid}`. Only used internally by `evtail-server`.
