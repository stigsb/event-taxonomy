# `evtail`

The `evtail` tool lets you "tail" the Kafka event stream in real time.

## Installation

To install `evtail`, you need to run this from a Zedge network (or VPN):

```bash
curl -s https://evtail-test.zedge.io/install | sh
```

This will install `evtail` in `/usr/local/bin`, but you can choose another destination directory by setting the `INSTALL_DIRECTORY` environment
variable:

```bash
curl -s https://evtail-test.zedge.io/install | INSTALL_DIRECTORY=$HOME/bin sh
```

## Usage

During development, if you have a single Android device or emulator connected to adb, simply run:

```bash
evtail --test
```

This will grab your zid from your device using `adb` and look for any new incoming events with that zid.

To watch for events from a release build, you need to find your zid somewhere (`adb logcat` should do the trick) and run:

```bash
evtail --zid=yourzid
```

## Command Line Options

Some useful command line options:

```txt
  -appid string
        Zedge App ID used to show properties that will not be stored (default "android")
  -package string
        Android package id (default "net.zedge.android")
  -colors
        colorize the output, even if stdout is not a terminal
  -d    whether to grab zid from the first Android device found
  -e    whether to get zid from the first Android emulator found
  -s string
        grab zid from attached Android device with this serial
  -taxman-url string
        URL to taxman service (default "https://taxman.zedge.io")
  -test
        whether to hit the test environment (defaults to prod)
  -url string
        websocket server URL (default "wss://evtail.zedge.io/ws")
  -version
        show version and exit
  -zid string
        which zid to watch messages for
```
