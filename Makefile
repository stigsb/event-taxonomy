
# Produce CRDs that work back to Kubernetes 1.11 (no version conversion)
CRD_OPTIONS ?= "crd:trivialVersions=true"

CONTROLLER_GEN ?= go run sigs.k8s.io/controller-tools/cmd/controller-gen

all: test manager

.PHONY: generate manifests manager test coverage run install deploy manifests fmt vet all

TEST_PKGS=./...

# Run tests
test: #generate fmt vet manifests
	KUBEBUILDER_CONTROLPLANE_START_TIMEOUT=60s \
	go test -v -coverprofile cover.out $(TEST_PKGS) | tee test.out
	cat cover.out | ./hack/coverage-without-generated-code.sh > cover-nongen.out
	go tool cover -func cover-nongen.out | grep '^total:'

coverage: test
	go tool cover -html=cover-nongen.out

# Build manager binary
manager: generate fmt vet
	go build -o bin/manager ./cmd/manager

# Run against the configured Kubernetes cluster in ~/.kube/config
run: generate fmt vet
	go run ./cmd/manager/main.go

# Install CRDs into a cluster
install: manifests
	kubectl apply -f config/crds

# Deploy controller in the configured Kubernetes cluster in ~/.kube/config
deploy: manifests
	kubectl apply -f config/crds
	kustomize build config/default | kubectl apply -f -

# Generate manifests e.g. CRD, RBAC etc.
manifests:
	$(CONTROLLER_GEN) $(CRD_OPTIONS) rbac:roleName=manager-role webhook paths="./..." output:crd:artifacts:config=config/crd/bases

# Run go fmt against code
fmt:
	go fmt ./pkg/... ./cmd/...

# Run go vet against code
vet:
	go vet ./pkg/... ./cmd/...

# Generate code
generate:
	sed -e 's,^,// ,; s,  *$$,,' LICENSE.txt > hack/boilerplate.go.txt
	$(CONTROLLER_GEN) object:headerFile=./hack/boilerplate.go.txt paths="./..."
